#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-common.
#
# rmt-common is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-common is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-common. If not, see <https://www.gnu.org/licenses/>.

############################
# NOTE
############################

# the input method 'read-secret-interactive' cannot be tested
#   as its internal stty usage causes bats failures

############################
# INCLUDES
############################

# currently, dotfiles to test reside in the same directory as bats files
declare -r __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/../../getters.dotfile"
load "${__source_dir}/../../input.dotfile"
load "${__source_dir}/../../math.dotfile"
load "${__source_dir}/../../strings.dotfile"
load "${__source_dir}/../../types.dotfile"

############################
# GLOBAL DATA
############################

declare -r -g __test_data_dir=$(mktemp -d -u)
declare -r -g __test_data_file="${__test_data_dir}/test-data.txt"

############################
# TOOLS
############################

generate-test-file-content-singleline() {
  printf 'filein\n'
}

generate-test-file-content-multiline() {
  printf 'fileinA\nfileinB\nfileinC\n'
}

############################
# ASSERTS
############################

assert-equal-all-lines-unordered() {
# set -x
  declare -r -n listRef="$1"

  local found expectedValue availableValue
  while read -r expectedValue; do
    found=0
    for availableValue in "${listRef[@]}"; do
      [[ "${availableValue}" != "${expectedValue}" ]] && continue
      found=1
    done
    
    [[ ${found} -eq 0 ]] && {
      printf '# expected value [%s] not found\n' "${expectedValue}" >&3
      return 1
    }
  done
# set +x
  return 0
}

############################
# WRAPPERS
############################
# wrapper functions in order to test the setting of global variables
# which wont work when using bats run command, as run invocation is
# performed in a subshell which wont propagate changes to global
# variables

read-lines-key-value-from-stdin-wrapper() {
  local res=0
  ! read-lines-key-value-from-stdin "$@" && res=1

# set -x
  declare -n resultMapRef="$1"
  
  local key value
  for key in "${!resultMapRef[@]}"; do
    value="${resultMapRef[${key}]}"
    if [[ -z "${value}" ]]; then
      printf '%s\n' "${key}"
    else
      printf '%s %s\n' "${key}" "${value}"
    fi
  done

# set +x
  return ${res}
}

read-from-singleline-stdin-wrapper() {
  ! read-from-singleline-stdin "$@" && return 1

  declare -n resultVarRef="$1"
  printf '%s\n' "${resultVarRef}"
  return 0
}

read-from-multiline-stdin-wrapper() {
  ! read-from-multiline-stdin "$@" && return 1

  declare -n resultsListRef="$1"
  [[ ${#resultsListRef[*]} -gt 0  ]] && printf '%s\n' "${resultsListRef[@]}"
  return 0
}

# for parameter usage see read-variables-from-multiline-stdin
# @return print all values stored in variables whose names are passed
#         as arguments, to stdout, one variable per line
read-variables-from-multiline-stdin-wrapper() {
  ! read-variables-from-multiline-stdin "$@" && return 1

  get-values-from-vars-indirectly "$@"
  return 0
}

# for parameter usage see read-from-identity-wrapper
# @return print the value stored in variable whose name is passed
#         as first argument, to stdout on a single line
read-from-identity-wrapper() {
  ! read-from-identity "$@" && return 1

  get-values-from-vars-indirectly "$1"
  return 0
}

# for parameter usage see read-from-fd-wrapper
# @return print the value stored in variable whose name is passed
#         as first argument, to stdout on a single line
read-from-fd-wrapper() {
  ! read-from-fd "$@" && return 1

  get-values-from-vars-indirectly "$1"
  # printf '%s\n' "${!1}"
  return 0
}

# for parameter usage see read-file-first-line
# @return print the value stored in variable whose name is passed
#         as first argument, to stdout on a single line
read-file-first-line-wrapper() {
  ! read-file-first-line "$@" && return 1

  get-values-from-vars-indirectly "$1"
  return 0
}

# for parameter usage see read-file-all
# @return print the values stored in array variable whose name
#          is passed as first argument, to stdout on a single line,
#          values are separated by whitespaces
read-file-all-wrapper() {
  ! read-file-all "$@" && return 1

  declare -n res="${1}"
  printf '%s\n' "${res[@]}"
  return 0
}

read-singleline-sequence() {
  # 1 value from stdin is stored in 1 variable var
  read-from-singleline-stdin var

  printf '%s\n' $?
  printf '%s\n' "${var}"

  # 1 value from stdin is stored in 1 variable var
  read-from-singleline-stdin var

  printf '%s\n' $?
  printf '%s\n' "${var}"
}

read-multiline-sequence() {
  read-from-multiline-stdin varList 2

  printf '%s\n' $?
  printf '%s\n' "${varList[0]}"
  printf '%s\n' "${varList[1]}"

  # added to existing entries in varList
  read-from-multiline-stdin varList 2

  printf '%s\n' $?
  printf '%s\n' "${varList[2]}"
  printf '%s\n' "${varList[3]}"
}

############################
# SETUP
############################

setup() {
  [[ -d "${__test_data_dir}" ]] && rm -r "${__test_data_dir}"
  mkdir -p "${__test_data_dir}"
}

teardown() {
  [[ -d "${__test_data_dir}" ]] && rm -r "${__test_data_dir}"
}

############################
# Tests
############################

@test "invoke read-file-all" {

  local array=()
  generate-test-file-content-multiline > "${__test_data_file}"

  # should write values from filein to array
  # array content is printed to stdout and is captured
  # as single string in output variable
  # run read-file-all-wrapper array "${fileIn}"
  run read-file-all-wrapper array "${__test_data_file}"

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == "fileinA" ]
  [ "${lines[1]}" == "fileinB" ]
  [ "${lines[2]}" == "fileinC" ]
}

@test "invoke read-file-all - limited lines" {

  local array=()
  generate-test-file-content-multiline > "${__test_data_file}"

  # should write values from filein to array
  # array content is printed to stdout and is captured
  # as single string in output variable
  run read-file-all-wrapper array "${__test_data_file}" 1

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == "fileinA" ]
  [ -z "${lines[1]}" ]
  [ -z "${lines[2]}" ]
  [ "${#lines[*]}" -eq 1 ]
  
  run read-file-all-wrapper array "${__test_data_file}" 2

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == "fileinA" ]
  [ "${lines[1]}" == "fileinB" ]
  [ -z "${lines[2]}" ]
  [ "${#lines[*]}" -eq 2 ]
}

# Note: when testing invalid arguments there is no need
#         to specify a valid input file as function to be
#         tested wont read from that file at all but is
#         supposed to stop already on argument validation
@test "invoke read-file-all - invalid data - error - incomplete args" {

  # missing variable and file name
  run read-file-all-wrapper
  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # missing file name
  run read-file-all-wrapper arr
  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # maximum frour arguments are expected, not five
  run read-file-all-wrapper arr /tmp 1 1 unknowń
  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # /tmp/unknown does not exist
  run read-file-all-wrapper arr /tmp/unknown123987
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: not a file [/tmp/unknown123987]' ]

  # /tmp is not a file but a directory
  run read-file-all-wrapper arr /tmp
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: not a file [/tmp]' ]
}

@test "invoke read-file-all - invalid data - error - invalid args" {

  generate-test-file-content-singleline > "${__test_data_file}"

  # if third argument is provided, it must be a integer > ß
  run read-file-all-wrapper arr "${__test_data_file}" 0
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run read-file-all-wrapper arr "${__test_data_file}" -0
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run read-file-all-wrapper arr "${__test_data_file}" -1
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}
  
@test "invoke read-file-all - error - invalid data - bash exit" {

  generate-test-file-content-singleline > "${__test_data_file}"
  
  # invalid variable name
  # => bash exits with a runtime error
  # => status cannot be tested
  run read-file-all-wrapper -arr "${__test_data_file}"

  # printf 'l # %s\n' "${lines[@]}" >&3
  # [ ${status} -eq 1 ]
  [[ "${lines[0]}" =~ 'declare' ]]
}

@test "invoke read-file-first-line" {

  generate-test-file-content-singleline > "${__test_data_file}"

  # should write value from filein to var
  # var content is printed to stdout and is captured
  # as single string in output variable
  run read-file-first-line-wrapper var "${__test_data_file}"
  # printf 'l # %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == "filein" ]
}

# Note: when testing invalid arguments there is no need
#         to specify a valid input file as function to be
#         tested wont read from that file at all but is
#         supposed to stop already on argument validation
@test "invoke read-file-first-line - error - invalid arguments" {

  # missing variable and file name
  run read-file-first-line-wrapper
  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # missing file name
  run read-file-first-line-wrapper var
  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # maximum three arguments are expected, not four
  run read-file-first-line-wrapper var /tmp 1 unknown
  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # /tmp/unknown does not exist
  run read-file-first-line-wrapper var /tmp/unknown123987
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: not a file [/tmp/unknown123987]' ]

  # /tmp is not a file but a directory
  run read-file-first-line-wrapper var /tmp
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: not a file [/tmp]' ]
}

@test "invoke read-file-first-line - error - invalid arguments - bash exit" {

  generate-test-file-content-singleline > "${__test_data_file}"
  
  # -var is a invalid variable name
  # => causes bach to 3xit with a runtime errord
  run read-file-first-line-wrapper -var "${__test_data_file}"
 
  # printf 'l # %s\n' "${lines[@]}" >&3
  # [ ${status} -eq 1 ]
  [[ "${output}" =~ 'declare' ]]
  
  run read-file-first-line-wrapper '' "${__test_data_file}"
  [[ "${output}" =~ 'declare' ]]
}

@test "invoke read-lines-key-value-from-stdin - no data" {
  declare -A kvMap=()

  run read-lines-key-value-from-stdin-wrapper kvMap

  [ ${status} -eq 0 ]
  [ ${#lines[*]} == 0 ]
}

@test "invoke read-lines-key-value-from-stdin - single line" {
  declare -A kvMap=()
  
  run read-lines-key-value-from-stdin-wrapper kvMap <<<'key1 value1'
  # printf 'l # %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [[ "${lines[0]}" == 'key1 value1' ]]
}

@test "invoke read-lines-key-value-from-stdin - multiline line - unique" {
  declare -A kvMap=()
  
  run read-lines-key-value-from-stdin-wrapper kvMap <<EOF
key1 value1
key2 value2
key3 value3
EOF
  # printf 'l # %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 3 ]
  
  run assert-equal-all-lines-unordered lines<<EOF
key1 value1
key2 value2
key3 value3
EOF
  # printf 'l # %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
}

@test "invoke read-lines-key-value-from-stdin - multiline line - dublicates" {
  declare -A kvMap=()
  
  run read-lines-key-value-from-stdin-wrapper kvMap <<EOF
key1 value1
key2 value2
key3 value3
key1 value4
key3 value5
EOF
  # printf 'l # %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ ${#lines[*]} -eq 3 ]
  
  run assert-equal-all-lines-unordered lines<<EOF
key1 value4
key2 value2
key3 value5
EOF

  [ ${status} -eq 0 ]
}

@test "invoke read-lines-key-value-from-stdin - error - no arguments" {
  # no map argument
  run read-lines-key-value-from-stdin-wrapper
  [ ${status} -eq 1 ]
}

@test "invoke read-lines-key-value-from-stdin - error - invalid arguments - bash exit" {

  # invalid map name
  run read-lines-key-value-from-stdin-wrapper -kvMap
  # printf 'l2 # %s\n' "${lines[@]}" >&3

  # [ ${status} -eq 1 ]
  [[ "${output}" =~ declare ]]
}

@test "invoke read-lines-key-value-from-stdin - multiline - error - missing values" {
  declare -A kvMap=()
  
  # lines without key value pair are ignored and cause an error return code
  run read-lines-key-value-from-stdin-wrapper kvMap <<EOF
key1
key2 value2
key3
key3 value5
EOF
#   printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ ${#lines[*]} -eq 2 ]
  
  run assert-equal-all-lines-unordered lines<<EOF
key2 value2
key3 value5
EOF

  [ ${status} -eq 0 ]
}

@test "invoke read-lines-key-value-from-stdin - multiline - error - keys only" {
  declare -A kvMap=()
  
  run read-lines-key-value-from-stdin-wrapper kvMap <<EOF
key1
key2
key3
EOF
  # printf 'l # %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ ${#lines[*]} -eq 0 ]
}

@test "invoke read-lines-key-value-from-stdin - single line - error - keys only" {
  declare -A kvMap=()
  
  run read-lines-key-value-from-stdin-wrapper kvMap <<< 'key1'
  # printf 'l # %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ ${#lines[*]} -eq 0 ]
}

@test "invoke read-from-fd" {
  # string 'fdin' is piped via file descriptor 10 to function
  # and is supposed to be stored in var. content of var is written
  # to stdout and is captured by output variable
  run read-from-fd-wrapper var 10 10< <(printf 'fdin')
  # printf 'l1 # %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == "fdin" ]

  # the same test as above, with linefeeas above, d
  run read-from-fd-wrapper var 10 10< <(printf 'fdin\n')
  # printf 'l2 # %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == "fdin" ]

  # test if whitespaces are read
  run read-from-fd-wrapper var 10 10< <(printf '  fdin  ')

  [ ${status} -eq 0 ]
  [ "${output}" == "  fdin  " ]

  # test if whitespaces are read
  run read-from-fd-wrapper var 10 10< <(printf '  fdin  fdin')

  [ ${status} -eq 0 ]
  [ "${output}" == "  fdin  fdin" ]

  # test if only the first line is read
  run read-from-fd-wrapper var 5 5< <(printf 'pwdA\npwdB\n')

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == "pwdA" ]
  [ ${#lines[*]} -eq 1 ]
  
  # empty data on fd
  run read-from-fd-wrapper var 5 5< <(printf '')
  # printf '# l3 [%s]\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  # only a linefeed on fd
  run read-from-fd-wrapper var 5 5< <(printf '\n')
  # printf '# l4 [%s]\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke read-from-fd - error - invalid data" {

  # fd 5 does not exist
  run read-from-fd-wrapper var 5
  # printf 'l1 # %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke read-from-fd - error - invalid arguments" {

  run read-from-fd-wrapper
  # printf 'l1 # %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # missing file descriptor
  run read-from-fd-wrapper var

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run read-from-fd-wrapper var a
  # printf 'l2 # %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  # invalid file descriptor
  run read-from-fd-wrapper var 1a
  # printf 'l2 # %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run read-from-fd var -1
  # printf 'l3 # %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  # can fd 0 exist at all?
  run read-from-fd-wrapper var 0
  # printf 'l4 # %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # max 3 arguments are expected, but 4 are provided
  run read-from-fd-wrapper var 1 1 unknown

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke read-from-fd - error - invalid arguments - bash exit" {
  
  # invalid variable name '-var'
  # => bash exits with a runtime error
  run read-from-fd -var 3
  # printf 'l1 # %s\n' "${lines[@]}" >&3

  # [ ${status} -eq 1 ]
  [[ "${output}" =~ 'declare' ]]
}

@test "invoke read-from-multiline-stdin" {
  
  local stdinList=()
  run read-from-multiline-stdin-wrapper stdinList <<< stdin

  [ ${status} -eq 0 ]
  [ "${#lines[@]}" -eq 1 ]
  [ "${lines[0]}" == "stdin" ]

  # read one value from stdin but also limit number of
  # values read from stdin to 1
  stdinList=()
  run read-from-multiline-stdin-wrapper stdinList 1 <<< stdin

  [ ${status} -eq 0 ]
  [ "${#lines[@]}" -eq 1 ]
  [ "${lines[0]}" == "stdin" ]

  # store three values from stdin in array
  stdinList=()
  run read-from-multiline-stdin-wrapper stdinList <<EOF
stdin
multi
line
EOF

  [ ${status} -eq 0 ]
  [ "${#lines[@]}" -eq 3 ]
  [ "${lines[0]}" == "stdin" ]
  [ "${lines[1]}" == "multi" ]
  [ "${lines[2]}" == "line" ]

  # store three values from stdin in array, and limit number of
  # variables from stdin to 1
  stdinList=()
  run read-from-multiline-stdin-wrapper stdinList 1 <<EOF
stdin
multi
line
EOF

  [ ${status} -eq 0 ]
  [ "${#lines[@]}" -eq 1 ]
  [ "${lines[0]}" == "stdin" ]

  # store three values from stdin in array, and limit number of
  # variables from stdin to 2
  stdinList=()
  run read-from-multiline-stdin-wrapper stdinList 2 <<EOF
stdin
multi
line
EOF

  [ ${status} -eq 0 ]
  [ "${#lines[@]}" -eq 2 ]
  [ "${lines[0]}" == "stdin" ]
  [ "${lines[1]}" == "multi" ]
}

@test "invoke read-from-multiline-stdin - multiple times" {

  run read-multiline-sequence <<EOF
stdinA
stdinAA
stdinB
stdinBB
EOF

  [ "${lines[0]}" -eq 0 ]
  [ "${lines[1]}" == "stdinA" ]
  [ "${lines[2]}" == "stdinAA" ]

  [ "${lines[3]}" -eq 0 ]
  [ "${lines[4]}" == "stdinB" ]
  [ "${lines[5]}" == "stdinBB" ]
}

@test "invoke read-from-multiline-stdin - error - invalid data - bash exit" {
  # invalid variable name
  # => bash exits with a runtime error
  # => status cannot be tested
  run read-from-multiline-stdin -arr 1 <<< stdin
  # printf 'l1 %s\n' "${lines[@]}"
  # [ ${status} -eq 1 ]
  [[ "${output}" =~ 'declare' ]]
}

@test "invoke read-from-multiline-stdin - error - invalid arguments" {

  # missing variable name
  run read-from-multiline-stdin <<< stdin

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # expects at most 3 parameters, but 4 are provided
  run read-from-multiline-stdin array 1 1 x <<< stdin
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  # optional argument must be larger 0
  run read-from-multiline-stdin array 0 <<< stdin
  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # invalid optional argument, should be a number
  run read-from-multiline-stdin array a <<< stdin

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # invalid optional argument, should be a number
  run read-from-multiline-stdin array 1a <<< stdin

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke read-from-multiline-stdin - error - invalid data" {

  # data on stdin is not sufficient to read two lines
  run read-from-multiline-stdin array 2 <<< stdin
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: at least [2] lines must have been read but only got [1]' ]

  # data on stdin contains empty lines
  run read-from-multiline-stdin array 4 <<EOF
stdin
multi

line
EOF

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke read-from-singleline-stdin" {

  # 1 value from stdin is stored in 1 variable var
  run read-from-singleline-stdin-wrapper var <<< stdin

  [ ${status} -eq 0 ]
  [ "${output}" == "stdin" ]

  # 1 value from stdin is stored in 1 variable var
  run read-from-singleline-stdin-wrapper var <<<'stdin with space'

  [ ${status} -eq 0 ]
  [ "${output}" == "stdin with space" ]
  
  # 1 value from stdin is stored in 1 variable var
  run read-from-singleline-stdin-wrapper var <<<''

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke read-from-singleline-stdin - multiple times" {

  run read-singleline-sequence <<EOF
stdinA
stdinB
EOF

  [ "${lines[0]}" -eq 0 ]
  [ "${lines[1]}" == "stdinA" ]
  [ "${lines[2]}" -eq 0 ]
  [ "${lines[3]}" == "stdinB" ]

}

@test "invoke read-from-singleline-stdin - error - invalid arguments" {
  # missing variable name(s)
  run read-from-singleline-stdin <<< stdin

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke read-from-singleline-stdin - error - invalid arguments - bash exit" {

  # invalid variable name
  run read-from-singleline-stdin -var <<< stdin
  # printf '# l %s\n' "${lines[@]}" >&3

  # [ ${status} -eq 1 ]
  [[ "${output}" =~ 'declare' ]]
}

@test "invoke read-from-identity" {

  # variable should contain 'identity' string
  #   variable content is written to stdout and is captured
  #   by output variable
  run read-from-identity-wrapper var "identity"

  [ ${status} -eq 0 ]
  [ "${output}" == "identity" ]

  # variable should be empty
  #   variable content is written to stdout and is captured
  #   by output variable
  run read-from-identity-wrapper var ""

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke read-from-identity - error - invalid data" {

  # expects only two arguments but 3 are provided
  run read-from-identity-wrapper var "" "tests"

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # missing data
  run read-from-identity-wrapper var

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # missing variable name and data
  run read-from-identity-wrapper

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke read-from-identity - error - invalid data - bash exit" {
  # invalid variable name
  run read-from-identity-wrapper -var "identity"
  # printf 'l # %s\n' "${lines[@]}" >&3

  # [ ${status} -eq 1 ]
  [[ "${output}" =~ 'declare' ]]
}

#
# NOTE we cannot test variable content here directly as run command spans a
#   subshell in which changes of variables are not propagated beyond the subshell
@test "invoke read-variables-from-multiline-stdin" {

  run read-variables-from-multiline-stdin-wrapper varA <<< aaa

  [ ${status} -eq 0 ]
  [ "${output}" == 'aaa' ]
  
  run read-variables-from-multiline-stdin-wrapper varA varB <<EOF
aaa
bbb
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'aaa' ]
  [ "${lines[1]}" == 'bbb' ]
  
  run read-variables-from-multiline-stdin-wrapper varA varB <<EOF
aaa
bbb
ccc
EOF

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'aaa' ]
  [ "${lines[1]}" == 'bbb' ]
}

@test "invoke read-variables-from-multiline-stdin - error - missing data" {

  run read-variables-from-multiline-stdin-wrapper varA varB varC <<EOF
aaa
bbb
EOF

  # printf 'l # %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: at least [3] lines must have been read but only got [2]' ]
  
  run read-variables-from-multiline-stdin-wrapper varA varB varC

  # printf 'l # %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: at least [3] lines must have been read but only got [0]' ]
}

@test "invoke read-variables-from-multiline-stdin - error - missing arguments" {

  # skip 'to be implemneted'
  
}


@test "invoke read-variables-from-multiline-stdin - error - invalid arguments - bash exit" {

  # invalid variable name '-varA'
  run read-variables-from-multiline-stdin-wrapper -varA <<< aaa
  # printf 'l1 # %s\n' "${lines[@]}" >&3

  # [ ${status} -eq 1 ]
  [[ "${output}" =~ 'declare' ]]
  
  # invalid variable name '-varB'
  run read-variables-from-multiline-stdin-wrapper varA -varB <<EOF
aaa
bbb
EOF
  # printf 'l2 # %s\n' "${lines[@]}" >&3

  # [ ${status} -eq 1 ]
  [[ "${output}" =~ 'declare' ]]
}


@test "invoke generate-read-from-fd-cmds" {

  run generate-read-from-fd-cmds var 3

  [ ${status} -eq 0 ]
  [ "${output}" == "read-from-fd var 3" ]

  # expect three commands for given fd's
  run generate-read-from-fd-cmds var 3 5 40
  
  [ ${status} -eq 0 ]
  [ ${#lines[@]} -eq 3 ]
  [ "${lines[0]}" == "read-from-fd var 3" ]
  [ "${lines[1]}" == "read-from-fd var 5" ]
  [ "${lines[2]}" == "read-from-fd var 40" ]
}

@test "invoke generate-read-from-fd-cmds - error - missing arguments" {

  # missing var name and fd num 
  run generate-read-from-fd-cmds

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # missing fd num 
  run generate-read-from-fd-cmds var

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # 'a' is not a valid fd
  run generate-read-from-fd-cmds var a

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # 'a' is not a valid fd
  run generate-read-from-fd-cmds var 1 a 10

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'read-from-fd var 1' ]
  [ "${lines[1]}" == 'read-from-fd var 10' ]
}

@test "invoke generate-read-from-fd-cmds - error - invalid arguments  - bash exit" {

  run generate-read-from-fd-cmds -var 3
  # printf 'l # %s\n' "${lines[@]}" >&3

  # [ ${status} -eq 1 ]
  [[ "${output}" =~ 'declare' ]]
}

@test "invoke generate-read-file-first-line-cmd" {

  run generate-read-file-first-line-cmd var /tmp/pwd

  [ ${status} -eq 0 ]
  [ "${output}" == "read-file-first-line var /tmp/pwd" ]

  # expect three commands for given file pathes
  run generate-read-file-first-line-cmd var /tmp/pwdA /tmp/pwdB /tmp/pwdC

  [ ${status} -eq 0 ]
  [ ${#lines[@]} -eq 3 ]
  [ "${lines[0]}" == "read-file-first-line var /tmp/pwdA" ]
  [ "${lines[1]}" == "read-file-first-line var /tmp/pwdB" ]
  [ "${lines[2]}" == "read-file-first-line var /tmp/pwdC" ]
}

@test "invoke generate-read-file-first-line-cmd - error - missing arguments" {
  run generate-read-file-first-line-cmd

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run generate-read-file-first-line-cmd var

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke generate-read-file-first-line-cmd - error - invalid arguments - bash exit" {

  run generate-read-file-first-line-cmd -var somefile

  # [ ${status} -eq 1 ]
  [[ "${output}" =~ 'declare' ]]
}

@test "invoke generate-read-secrets-interactive-cmds" {

  # expect one command with default label
  run generate-read-secrets-interactive-cmds var
  
  [ ${status} -eq 0 ]
  [ "${output}" == "read-secret-interactive-labeled-default var" ]

  # expect one command with default label
  run generate-read-secrets-interactive-cmds var 1

  [ ${status} -eq 0 ]
  [ "${output}" == "read-secret-interactive-labeled-default var" ]

  # expect one command with custom label
  run generate-read-secrets-interactive-cmds var 1 "label"

  [ ${status} -eq 0 ]
  [ "${output}" == "read-secret-interactive-labeled-single label var" ]

  # expect one command with custom label
  run generate-read-secrets-interactive-cmds var 1 "http://example.com"

  [ ${status} -eq 0 ]
  [ "${output}" == "read-secret-interactive-labeled-single http://example.com var" ]

  # expect one command with default label
  run generate-read-secrets-interactive-cmds var 1 "labelA" "labelB" "labelC"
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == "read-secret-interactive-labeled-single labelA var" ]
  
  # expect three commands with individual labels
  run generate-read-secrets-interactive-cmds var 2 "labelA" "labelB" "labelC"
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ ${#lines[@]} -eq 2 ]
  [ "${lines[0]}" == "read-secret-interactive-labeled-single labelA var" ]
  [ "${lines[1]}" == "read-secret-interactive-labeled-single labelB var" ]
  
  # expect three commands with individual labels
  run generate-read-secrets-interactive-cmds var 3 "labelA" "labelB" "labelC"

  [ ${status} -eq 0 ]
  [ ${#lines[@]} -eq 3 ]
  [ "${lines[0]}" == "read-secret-interactive-labeled-single labelA var" ]
  [ "${lines[1]}" == "read-secret-interactive-labeled-single labelB var" ]
  [ "${lines[2]}" == "read-secret-interactive-labeled-single labelC var" ]
}

@test "invoke generate-read-secrets-interactive-cmds - error - missing arguments" {

  # missing variable name
  run generate-read-secrets-interactive-cmds

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # invalid number of labels, two labels must be given
  run generate-read-secrets-interactive-cmds var 2 "label"

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: number of labels [1] must be at least equal to the number of passwords to be prompted for [2]' ]
}

@test "invoke generate-read-secrets-interactive-cmds - error - invalid arguments" {

  # invalid number of prompts, cannot be 0
  run generate-read-secrets-interactive-cmds var 0

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # invalid number of prompts, must be a number
  run generate-read-secrets-interactive-cmds var a

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # invalid number of prompts, cannot be negative
  run generate-read-secrets-interactive-cmds var -1

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  # invalid number of prompts, must be a number
  run generate-read-secrets-interactive-cmds var 1a

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke generate-read-secrets-interactive-cmds - error - invalid arguments - bash exit" {

  # invalid variable name
  run generate-read-secrets-interactive-cmds -var
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [[ "${output}" =~ 'declare' ]]
}

