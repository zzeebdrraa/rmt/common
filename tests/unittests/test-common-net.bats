#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-common.
#
# rmt-common is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-common is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-common. If not, see <https://www.gnu.org/licenses/>.

############################
# INCLUDES
############################

# currently, dotfiles to test reside in the same directory as bats files
declare -g -r __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/../../math.dotfile"
load "${__source_dir}/../../net.dotfile"

############################
# TEST DATA
############################

declare -g __netcat_counter=0

############################
# MOCKS
############################

# always return 0 and increases counter
# counter will be 1
netcat-reachable-mock() {
  __netcat_counter=$((__netcat_counter+1))
  return 0
}

# never return 0 and increases counter
# counter will be 1
netcat-not-reachable-mock() {
  __netcat_counter=$((__netcat_counter+1))
  return 1
}

# return 0 when being invoked for 5 times and always increases counter
# counter will be 5
netcat-reachable-mock-5() {
  __netcat_counter=$((__netcat_counter+1))
  [[ ${__netcat_counter} -eq 5 ]] && return 0
  return 1
}

# return 0 when being invoked for 5 times and always increases counter
# counter will be 5
netcat-not-reachable-mock-5() {
  __netcat_counter=$((__netcat_counter+1))
  [[ ${__netcat_counter} -eq 5 ]] && return 1
  return 0
}

############################
# UTILTITIES
############################

# a wrapper function to print the number of connection retries
# stored in global variable __netcat_counter
wait-for-host-to-be-reachable-wrapper() {
  wait-until-host-is-reachable "$@"
  local -r res=$?
  printf '%s\n' "${__netcat_counter}"
  return ${res}
}

# a wrapper function to print the number of connection retries
# stored in global variable __netcat_counter
wait-for-host-to-be-not-reachable-wrapper() {
  wait-until-host-not-reachable "$@"
  local -r res=$?
  printf '%s\n' "${__netcat_counter}"
  return ${res}
}

############################
# TESTS
############################

@test "invoke wait-until-host-is-reachable" {

  __netcat_cmd=netcat-reachable-mock

  # expects host to be reachable after 2 host request
  run wait-for-host-to-be-reachable-wrapper "example.com" 80 1 0
  # printf '# %s\n' "${lines[@]}" >&3

  [ "${status}" -eq 0 ]
  [ "${output}" == "1" ]

  __netcat_counter=0
  __netcat_cmd=netcat-reachable-mock-5

  # expects host to be reachable after 5 host request
  run wait-for-host-to-be-reachable-wrapper "example.com" 80 5 0

  [ "${status}" -eq 0 ]
  [ "${output}" == "5" ]

  __netcat_counter=0
  __netcat_cmd=netcat-not-reachable-mock

  # expects host not to be reachable after 50 host request
  run wait-for-host-to-be-reachable-wrapper "example.com" 80 50 0

  [ "${status}" -eq 1 ]
  [ "${output}" == "50" ]
}

@test "invoke wait-until-host-is-reachable - invalid args - error" {
  # missing address and port
  run wait-for-host-to-be-reachable-wrapper
  [ "${status}" -eq 1 ]

  # missing port
  run wait-for-host-to-be-reachable-wrapper "example.com"
  [ "${status}" -eq 1 ]

  # invalid port
  run wait-for-host-to-be-reachable-wrapper "example.com" 80a
  [ "${status}" -eq 1 ]

  # invalid number of maximum retries
  run wait-for-host-to-be-reachable-wrapper "example.com" 80 0
  [ "${status}" -eq 1 ]

  # invalid number of maximum retries
  run wait-for-host-to-be-reachable-wrapper "example.com" 80 -1
  [ "${status}" -eq 1 ]

  # invalid number of maximum retries
  run wait-for-host-to-be-reachable-wrapper "example.com" 80 1a 0
  [ "${status}" -eq 1 ]

  # invalid wait time between retries
  run wait-for-host-to-be-reachable-wrapper "example.com" 80 1 -1
  [ "${status}" -eq 1 ]

  # invalid wait stime between retries
  run wait-for-host-to-be-reachable-wrapper "example.com" 80 1 1a
  [ "${status}" -eq 1 ]
}


@test "invoke wait-until-host-not-reachable" {

  __netcat_cmd=netcat-not-reachable-mock

  # expects host to be reachable after 2 host request
  run wait-for-host-to-be-not-reachable-wrapper "example.com" 80 1 0
  # printf '# %s\n' "${lines[@]}" >&3

  [ "${status}" -eq 0 ]
  [ "${output}" == "1" ]

  __netcat_counter=0
  __netcat_cmd=netcat-not-reachable-mock-5

  # expects host to be reachable after 5 host request
  run wait-for-host-to-be-not-reachable-wrapper "example.com" 80 5 0

  [ "${status}" -eq 0 ]
  [ "${output}" == "5" ]

  __netcat_counter=0
  __netcat_cmd=netcat-reachable-mock

  # expects host not to be reachable after 50 host request
  run wait-for-host-to-be-not-reachable-wrapper "example.com" 80 50 0

  [ "${status}" -eq 1 ]
  [ "${output}" == "50" ]
}

@test "invoke wait-until-host-not-reachable - invalid args - error" {
  # missing address and port
  run wait-for-host-to-be-not-reachable-wrapper
  [ "${status}" -eq 1 ]

  # missing port
  run wait-for-host-to-be-not-reachable-wrapper "example.com"
  [ "${status}" -eq 1 ]

  # invalid port
  run wait-for-host-to-be-not-reachable-wrapper "example.com" 80a
  [ "${status}" -eq 1 ]

  # invalid number of maximum retries
  run wait-for-host-to-be-not-reachable-wrapper "example.com" 80 0
  [ "${status}" -eq 1 ]

  # invalid number of maximum retries
  run wait-for-host-to-be-not-reachable-wrapper "example.com" 80 -1
  [ "${status}" -eq 1 ]

  # invalid number of maximum retries
  run wait-for-host-to-be-not-reachable-wrapper "example.com" 80 1a 0
  [ "${status}" -eq 1 ]

  # invalid wait time between retries
  run wait-for-host-to-be-not-reachable-wrapper "example.com" 80 1 -1
  [ "${status}" -eq 1 ]

  # invalid wait stime between retries
  run wait-for-host-to-be-not-reachable-wrapper "example.com" 80 1 1a
  [ "${status}" -eq 1 ]
}

