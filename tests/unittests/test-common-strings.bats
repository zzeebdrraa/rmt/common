#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-common.
#
# rmt-common is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-common is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-common. If not, see <https://www.gnu.org/licenses/>.

############################
# INCLUDES
############################

# currently, dotfiles to test reside in the same directory as bats files
declare -g -r __source_dir="${BATS_TEST_DIRNAME}"

load ${__source_dir}/../../getters.dotfile
load ${__source_dir}/../../strings.dotfile
load ${__source_dir}/../../types.dotfile
load ${__source_dir}/../../math.dotfile

############################
# wrapper functions in order to test the setting of global variables
# which wont work when using bats run command, as run invoction is
# performed in a subshell which wonnt propagate changes to global
# variables
############################

# for parameter usage see  symmetrically-trim-quotes-indirection
#
# @return print all values stored in variables whose names are
#   passed as arguments, to stdout on a single line, values separated
#   by whitespaces
symmetrically-trim-quotes-indirection-test-wrapper() {
  ! symmetrically-trim-quotes-indirection "$@" && return 1

  get-values-from-vars-indirectly "$@"
  return 0
}

############################
# TEST DATA
############################

declare -g __test_var_with_quotes_lr='"quoteslr"'
declare -g __test_var_with_quotes_l='quotesl"'
declare -g __test_var_with_quotes_r='quotesr"'

declare -g __test_var_no_quotes_a='noquotesa'
declare -g __test_var_no_quotes_b='noquotesb'

############################
# TESTS
############################

###########

@test "invoke symmetrically-trim-quotes-indirection - variable name pointing to value without quotes" {

  # variable content is expected to remain as is
  # as it does not contain leading and trailing quotation marks
  # variable name is valid if it contains underscores
  run symmetrically-trim-quotes-indirection-test-wrapper __test_var_no_quotes_a __test_var_no_quotes_b
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'noquotesa' ]
  [ "${lines[1]}" == 'noquotesb' ]
}

@test "invoke symmetrically-trim-quotes-indirection - invalid arguments - error" {

  # missing variable name
  run symmetrically-trim-quotes-indirection-test-wrapper

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # invalid variable name
  run symmetrically-trim-quotes-indirection-test-wrapper ' '

  [ ${status} -eq 1 ]
  # [ -z "${output}" ]

  # invalid variable name
  run symmetrically-trim-quotes-indirection-test-wrapper ' a'

  [ ${status} -eq 1 ]
  # [ -z "${output}" ]

  # invalid variable name
  run symmetrically-trim-quotes-indirection-test-wrapper 'a '

  [ ${status} -eq 1 ]
  # [ -z "${output}" ]

  # invalid variable name
  run symmetrically-trim-quotes-indirection-test-wrapper 10

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # invalid variable name
  run symmetrically-trim-quotes-indirection-test-wrapper var10

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

###########

@test "invoke symmetrically-trim-char - variable containing quotes - left and right" {

  # expects leading and trailing quotation marks to be stripped from content of variable __test_var_with_quotes_lr
  run symmetrically-trim-char "${__test_var_with_quotes_lr}" '"'

  [ ${status} -eq 0 ]
  [ "${output}" == 'quoteslr' ]
}

@test "invoke symmetrically-trim-char - variable containing quotes - left or right" {

  run symmetrically-trim-char "${__test_var_with_quotes_l}" '"'

  [ ${status} -eq 0 ]
  [ "${output}" == 'quotesl' ]

  run symmetrically-trim-char "${__test_var_with_quotes_r}" '"'

  [ ${status} -eq 0 ]
  [ "${output}" == 'quotesr' ]
}

@test "invoke symmetrically-trim-char - variable contains no quotes" {

  # expects content of variable __test_var_no_quotes_a to remain as is
  run symmetrically-trim-char "${__test_var_no_quotes_a}" '"'

  [ ${status} -eq 0 ]
  [ "${output}" == 'noquotesa' ]
}

@test "invoke symmetrically-trim-char - invalid arguments - error" {

  # missing string and cutset argument
  run symmetrically-trim-char

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # missing cutset argument
  run symmetrically-trim-char "a"

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # expected cutset contains no chars
  run symmetrically-trim-char "a" ""

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # expected cutset contains more than 1 char
  run symmetrically-trim-char "a" "12"

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # expected string is empty
  run symmetrically-trim-char '' "1"

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

###########

@test "invoke starts-with - match" {

  # single char
  run starts-with 'some value' 's'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  # string
  run starts-with 'some value' 'some'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  # regex
  run starts-with 'some value' 'so[m]'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  # also works
  run starts-with 'some value' '^some value$'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke starts-with - no match" {

  # single char
  run starts-with 'some value' 'd'

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  # string
  run starts-with 'some value' 'sone'

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  # regex
  run starts-with 'some value' 'so[l]'

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  # regex
  run starts-with 'some value' 'value'

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke starts-with - missing args - error" {

  # missing args
  run starts-with

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run starts-with 'some value'

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

###########

@test "invoke concat-str - no args" {

  # missing delimiter
  run concat-str
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
    # missing parts
  run concat-str '-'
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke concat-str - empty delimiter" {

  run concat-str '' 'one'
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == 'one' ]

  run concat-str '' 'one' 'two'
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == 'onetwo' ]
}

@test "invoke concat-str - one part" {

  # only one part
  run concat-str '-' 'one'
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}" == 'one' ]
}

@test "invoke concat-str - one part - empty" {

  run concat-str '-' ''
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run concat-str '' ''
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke concat-str - concat - more parts" {

  run concat-str '----' 'one' 'two' 'three'
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}"  == 'one----two----three' ]
}

@test "invoke concat-str - concat - more parts - empty only" {

  run concat-str '----' '' '' ''
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run concat-str '' '' '' ''
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke concat-str - concat - more parts - partly empty" {

  run concat-str '----' 'one' '' 'three'
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}"  == 'one----three' ]
  
  run concat-str '' 'one' '' 'three'
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ "${output}"  == 'onethree' ]
}