#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-common.
#
# rmt-common is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-common is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-common. If not, see <https://www.gnu.org/licenses/>.

############################
# INCLUDES
############################

# currently, dotfiles to test reside in the same directory as bats files
declare -g -r __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/../../json.dotfile"
load "${__source_dir}/../../getters.dotfile"
load "${__source_dir}/../../utils.dotfile"

############################
# GLOBAL DATA
############################

declare -r -g __test_data_dir=$(mktemp -d -u)
declare -r -g __test_data_json_file="${__test_data_dir}/test-data.json"

############################
# HELPERS
############################

generate-test-data-json() {
  printf '%s' "
{
  \"name\":\"a name\",
  \"option\":\"\",
  \"list\":[
    0,
    00,
    000
  ],
  \"obj\":{
    \"day\":\"today\",
    \"year\":3000,
    \"loop\":false
  }
}"
}

############################
# SETUP
############################

setup() {
  [[ -d "${__test_data_dir}" ]] && rm -r "${__test_data_dir}"
  mkdir -p "${__test_data_dir}"
}

teardown() {
  [[ -d "${__test_data_dir}" ]] && rm -r "${__test_data_dir}"
}

############################
# TESTS
############################

@test "invoke try-get-value-from-json-file-or-var-or-default - invalid args" {

  generate-test-data-json >"${__test_data_json_file}"

  run try-get-value-from-json-file-or-var-or-default 'option' "${__test_data_json_file}" '  '
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run try-get-value-from-json-file-or-var-or-default 'option' '  '
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke try-get-value-from-json-file-or-var-or-default - invalid args - not yet detected" {

  skip 'not yet detected'

  generate-test-data-json >"${__test_data_json_file}"

  run try-get-value-from-json-file-or-var-or-default '  ' "${__test_data_json_file}" '  '
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke try-get-value-from-json-file-or-var-or-default - no args" {

  run try-get-value-from-json-file-or-var-or-default
  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run try-get-value-from-json-file-or-var-or-default ''
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke try-get-value-from-json-file-or-var-or-default - incomplete args" {

  generate-test-data-json >"${__test_data_json_file}"

  run try-get-value-from-json-file-or-var-or-default
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run try-get-value-from-json-file-or-var-or-default 'unknown'
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  # variable name is not specified
  run try-get-value-from-json-file-or-var-or-default 'unknown' "${__test_data_json_file}" 
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  # json field 'option' is a empty string
  # unknownVar does not hold any value
  # and default value has not been specified
  run try-get-value-from-json-file-or-var-or-default 'option' "${__test_data_json_file}" unknownVar
  # printf ' l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke try-get-value-from-json-file-or-var-or-default" {

  generate-test-data-json >"${__test_data_json_file}"

  # json field name holds a value
  run try-get-value-from-json-file-or-var-or-default 'name' "${__test_data_json_file}" 
  [ ${status} -eq 0 ]
  [ "${output}" == 'a name' ]

  run try-get-value-from-json-file-or-var-or-default 'name' "${__test_data_json_file}" aLocalVar 50
  [ ${status} -eq 0 ]
  [ "${output}" == 'a name' ]

  # json field option contains a empty string
  # aLocalVar contains data
  local aLocalVar='30'
  run try-get-value-from-json-file-or-var-or-default 'option' "${__test_data_json_file}"  aLocalVar
  [ ${status} -eq 0 ]
  [ "${output}" == '30' ]
  
  # json field option contains a empty string
  # aLocalVar contains an empty string
  # default value will be used
  local aLocalVar=''
  run try-get-value-from-json-file-or-var-or-default 'option' "${__test_data_json_file}"  aLocalVar 50
  [ ${status} -eq 0 ]
  [ "${output}" == '50' ]
  
  # json field option contains a empty string
  # unknownVar is not defined and will cause an error
  # default value will not be used
  run try-get-value-from-json-file-or-var-or-default 'option' "${__test_data_json_file}"  unknownVar 50
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}
