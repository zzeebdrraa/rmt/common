#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-common.
#
# rmt-common is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-common is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-common. If not, see <https://www.gnu.org/licenses/>.

############################
# NOTE
############################

# the common method read-secret-interactive canot be tested
#   as its internal stty usage causes bats failures

############################
# INCLUDES
############################

# currently, dotfiles to test reside in the same directory as bats files
declare -g -r __source_dir="${BATS_TEST_DIRNAME}"

load ${__source_dir}/../../passwords.dotfile

############################
# TESTS
############################

# NOTE the used regex pattern is erroneus as for some reason
#      a file gets created on local fs on test invocation
@test "invoke generate-password" {

  # generate password with default length
  run generate-password

  [ ${status} -eq 0 ]
  [ ${#output} -eq 40 ]
  [[ "${output}" =~ ^[a-zA-Z0-9]+$ ]]

  # generate password with length 5 chars
  run generate-password 5

  [ ${status} -eq 0 ]
  [ ${#output} -eq 5 ]
  [[ "${output}" =~ ^[a-zA-Z0-9]+$ ]]

  # generate password with length 1 char
  run generate-password 1

  [ ${status} -eq 0 ]
  [ ${#output} -eq 1 ]
  [[ "${output}" =~ ^[a-zA-Z0-9]+$ ]]
}

@test "invoke generate-password - invalid arguments - error" {

  # invalid password length, must be larger 0
  run generate-password 0

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # invalid password length, cannot be negative
  run generate-password -1

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # invalid password length, must be a number
  run generate-password a

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # invalid password length, must be a number
  run generate-password 1a

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}