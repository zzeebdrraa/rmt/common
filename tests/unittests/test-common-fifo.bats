#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-common.
#
# rmt-common is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-common is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-common. If not, see <https://www.gnu.org/licenses/>.

############################
# Includes
############################

# currently, dotfiles to test reside in the same directory as bats files
declare -g -r __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/../../fifo.dotfile"
load "${__source_dir}/../../math.dotfile"

############################
# GLOBAL DATA
############################

declare -r -g __test_data_dir=$(mktemp -d -u)
declare -r -g __test_fifo="${__test_data_dir}/test-fifo"

############################
# TOOLS
############################

generate-test-fifo-content-multiline() {
  printf 'line 1\nline 2\nline 3\n'
}

############################
# Setup
############################

setup() {
  [[ -d "${__test_data_dir}" ]] && rm -r "${__test_data_dir}"
  mkdir -p "${__test_data_dir}"
}

teardown() {
  [[ -d "${__test_data_dir}" ]] && rm -r "${__test_data_dir}"
}

############################
# Tests
############################

#***************************
# make-fifo
#***************************

@test "invoke make-fifo" {
  run make-fifo "${__test_fifo}"
  [ ${status} -eq 0 ]
  [ -p "${__test_fifo}" ]
}

@test "invoke make-fifo - no args" {
  run make-fifo
  [ ${status} -eq 1 ]
}

#***************************
# remove-fifo
#***************************

@test "invoke remove-fifo" {
  run make-fifo "${__test_fifo}"
  [ ${status} -eq 0 ]

  run remove-fifo "${__test_fifo}"
  [ ${status} -eq 0 ]
}

@test "invoke remove-fifo - no fifo" {
  run remove-fifo "${__test_fifo}"
  [ ${status} -eq 1 ]
}

@test "invoke remove-fifo - no args" {
  run remove-fifo
  [ ${status} -eq 1 ]
}

#***************************
# read-fifo
#***************************

@test "invoke read-fifo" {
  run make-fifo "${__test_fifo}"
  [ ${status} -eq 0 ]

  generate-test-fifo-content-multiline > "${__test_fifo}" &

  local fifoReadList=() res=0
  ! read-fifo "${__test_fifo}" fifoReadList && res=1
  [ ${res} -eq 0 ]

  [ "${fifoReadList[0]}" == 'line 1' ]
  [ "${fifoReadList[1]}" == 'line 2' ]
  [ "${fifoReadList[2]}" == 'line 3' ]
}

@test "invoke read-fifo - no args" {
  run read-fifo
  [ ${status} -eq 1 ]
}

@test "invoke read-fifo - no existing fifo" {
  run read-fifo "${__test_fifo}"
  [ ${status} -eq 1 ]
}

@test "invoke read-fifo - no list argument" {
  run make-fifo "${__test_fifo}"
  [ ${status} -eq 0 ]

  run read-fifo "${__test_fifo}"
  [ ${status} -eq 1 ]
}