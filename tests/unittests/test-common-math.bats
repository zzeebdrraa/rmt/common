#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-common.
#
# rmt-common is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-common is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-common. If not, see <https://www.gnu.org/licenses/>.

############################
# INCLUDES
############################

# currently, dotfiles to test reside in the same directory as bats files
declare -g -r __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/../../math.dotfile"

############################
# TEST DATA
############################

declare -r -g __is_larger_or_equal_invalid_integer_list=(
  '1,1 1,2'
  '1.2 1,1'
  '1,2 1.1'
  '1.1 1.2'
  '1, 2,'
  '1. 2,'
  '1, 2.'
  '1. 2.'
  '1, 2,'
  '1. 2,'
  '1, 2.'
  ',1 .2'
  '.1 ,2'
  ',1 ,2'
  '.1 .2'
)

declare -r -g __is_larger_or_equal_invalid_decimal_list=(
  '1,1 1,2'
  '1.2 1,1'
  '1,2 1.1'
  '1, 2,'
  '1. 2,'
  '1, 2.'
  ',1 .2'
  '.1 ,2'
  ',1 ,2'
  '1.1. 1.2.'
)

declare -r -g __is_larger_or_equal_true_positive_decimal_list=(
  # larger
  '2 1'
  '2. 1.'
  '1.2 1.1'
  '.2 .1'
  '.1 .01'
  '1.09 1.06'
  # equal
  '.00211 .00211'
  '.00021100 .00021100'
  '.002 .00200'
  '.0020 .002000'
  '.00211 .000211'
)

declare -r -g __is_larger_or_equal_false_positive_decimal_list=(
  # smaller
  '1 2'
  '1. 2.'
  '1.1 1.2'
  '.1 .2'
  '.01 .1'
  '.00021100 .000211001'
)

declare -r -g __is_larger_or_equal_true_negative_decimal_list=(
  # larger
  '-1.1 -1.2'
  '-.1 -.2'
  '-1. -2.'
  '-.004 -.005'
  '-.0004 -.005'
  '-.00400 -.00401'
  # equal
  '-.004 -.00400'
)

declare -r -g __is_larger_or_equal_false_negative_decimal_list=(
  # smaller
  '-.2 -.1'
  '-2. -1.'
  '-.005 -.004'
  '-.00401 -.00400'
)

declare -r -g __is_larger_or_equal_true_zero_decimal_list=(
  #equal
  '0 0'
  '.0 .0'
  '0 .0'
  '.0 0'
  '0. 0.'
  '0. 0'
  '0 0.'
  '-0 -0'
  '-0 0'
  '0 -0'
  '-0. -0.'
  '-0. 0.'
  '0. -0.'
  '-.0 -.0'
  '-.0 .0'
  '.0 -.0'
)

declare -r -g __is_larger_or_equal_true_zero_integer_list=(
  #equal
  '0 0'
  '-0 -0'
  '-0 0'
  '0 -0'
)

declare -r -g __is_larger_or_equal_true_positive_integer_list=(
  # larger
  '2 1'
  '2 -1'
  '0 -0'
  # equal
  '2 2'
  '-1 -1'
)

declare -r -g __is_larger_or_equal_false_positive_integer_list=(
  # smaller
  '1 2'
  '-1 2'
  '0 1'
  '-0 1'
  '-1 0'
  '-1 -0'
)

declare -r -g __is_larger_or_equal_true_negative_integer_list=(
  # larger
  '-1 -2'
  '2 -1'
  # equal
  '-2 -2'
  '-10 -10'
)

declare -r -g __is_larger_or_equal_false_negative_integer_list=(
  # smaller
  '-2 -1'
  '-1 2'
  '-2 1'
)

declare -r -g __is_digit_true_list=(
  0
  1
  2
  3
  4
  5
  6
  7
  8
  9
)

declare -r -g __is_digit_false_list=(
  .0
  1.
  -2
  33
)

declare -r -g __is_larger_zero_integer_true_list=(
  1
  10
  100
  10000000
)

declare -r -g __is_larger_zero_integer_false_list=(
  0
  -0
  -1
  -10
)

declare -r -g __is_larger_zero_integer_invalid_list=(
  10.3
  1.0
  -1.0
  -10.3
  .1
  .0
  a
  1a
  1o
  oo
  .
)

declare -r -g __is_positive_integer_true_list=(
  0
  1
  10
  100
  1000000
)

declare -r -g __is_positive_integer_false_list=(
  -1
  -10
)

declare -r -g __is_positive_integer_invalid_list=(
  -0
  0.0
  10.3
  1.0
  -1.0
  -10.3
  .1
  .0
  a
  1a
  1o
  oo
  .
)

declare -r -g __is_integer_true_list=(
  1000000
  1000
  100
  10
  1
  0
  0000000
  -00000
  -0
  -1
  -10
  -100
  -1000
  -1000000
)

declare -r -g __is_integer_false_list=(
  1.1
  1.0
  1.
  0.1
  .1
  a1
  1a
  0.0
  0.
  .0
  -0.1
  -1.0
  -1.
  -0.1
  -.1
  # dublicated negative sign
  --1
  ----1
)

declare -r -g __is_decimal_true_list=(
  100
  100.
  100.100
  .100
  .00100
  1
  1.1
  1.0
  1.
  0.1
  .1
  0.0
  0.
  .0
  -0.1
  -1.0
  -1.
  -0.1
  -.1
  -100
  -100.
  -100.100
  -.100
  -.00100
)

declare -r -g __is_decimal_false_list=(
  1a2
  a1
  1a
  100,
  100,100
  ,100
  ,00100
  1,0
  1,
  0,1
  ,1
  0,0
  0,
  ,0
  -0,1
  -1,0
  -1,
  -0,1
  -,1
  -100,
  -100,100
  -,100
  -,00100
  # dublicated negative sign
  --100
  --100.
  --100.00
  ----100
  ----100.
  ----100.00
)

############################
# TESTS
############################

#***************************
# is-larger-or-equal-integer
#***************************

@test "invoke is-larger-or-equal-integer - positive - true" {

  local entry valX valY
  for entry in "${__is_larger_or_equal_true_positive_integer_list[@]}"; do

    read -r valX valY <<<"${entry}"
    run is-larger-or-equal-integer "${valX}" "${valY}"

    [ ${status} -eq 0 ]
    [ -z "${output}" ]
  
  done
}

@test "invoke is-larger-or-equal-integer - positive - false" {

  local entry valX valY
  for entry in "${__is_larger_or_equal_false_positive_integer_list[@]}"; do

    read -r valX valY <<<"${entry}"
    run is-larger-or-equal-integer "${valX}" "${valY}"

    [ ${status} -eq 1 ]
    [ -z "${output}" ]
  
  done
}

@test "invoke is-larger-or-equal-integer - zero" {
  local entry valX valY
  for entry in "${__is_larger_or_equal_true_zero_integer_list[@]}"; do

    read -r valX valY <<<"${entry}"
    run is-larger-or-equal-integer "${valX}" "${valY}"

    [ ${status} -eq 0 ]
    [ -z "${output}" ]
  
  done
}

@test "invoke is-larger-or-equal-integer - negative - true" {
  local entry valX valY
  for entry in "${__is_larger_or_equal_true_negative_integer_list[@]}"; do

    read -r valX valY <<<"${entry}"
    run is-larger-or-equal-integer "${valX}" "${valY}"

    [ ${status} -eq 0 ]
    [ -z "${output}" ]
  
  done
}

@test "invoke is-larger-or-equal-integer - negative - false" {
  local entry valX valY
  for entry in "${__is_larger_or_equal_false_negative_integer_list[@]}"; do

    read -r valX valY <<<"${entry}"
    run is-larger-or-equal-integer "${valX}" "${valY}"

    [ ${status} -eq 1 ]
    [ -z "${output}" ]
  
  done
}

#***************************
# is-larger-or-equal-decimal
#***************************

@test "invoke is-larger-or-equal-decimal - zero" {
  local entry valX valY
  for entry in "${__is_larger_or_equal_true_zero_decimal_list[@]}"; do

    read -r valX valY <<<"${entry}"
    run is-larger-or-equal-decimal "${valX}" "${valY}"

    [ ${status} -eq 0 ]
    [ -z "${output}" ]
  
  done
}

@test "invoke is-larger-or-equal-decimal - negative - true" {
  local entry valX valY
  for entry in "${__is_larger_or_equal_true_negative_decimal_list[@]}"; do

    read -r valX valY <<<"${entry}"
    run is-larger-or-equal-decimal "${valX}" "${valY}"

    [ ${status} -eq 0 ]
    [ -z "${output}" ]
  
  done
}

@test "invoke is-larger-or-equal-decimal - negative - false" {
  local entry valX valY
  for entry in "${__is_larger_or_equal_false_negative_decimal_list[@]}"; do

    read -r valX valY <<<"${entry}"
    run is-larger-or-equal-decimal "${valX}" "${valY}"
    [ ${status} -eq 1 ]
    [ -z "${output}" ]
  
  done
}

@test "invoke is-larger-or-equal-decimal - positive - true" {

  local entry valX valY
  for entry in "${__is_larger_or_equal_true_positive_decimal_list[@]}"; do

    read -r valX valY <<<"${entry}"
    run is-larger-or-equal-decimal "${valX}" "${valY}"
    # printf '# %s\n' "${lines[@]}"

    [ ${status} -eq 0 ]
    [ -z "${output}" ]
  
  done
}

@test "invoke is-larger-or-equal-decimal - positive - false" {

  local entry valX valY
  for entry in "${__is_larger_or_equal_false_positive_decimal_list[@]}"; do

    read -r valX valY <<<"${entry}"
    run is-larger-or-equal-decimal "${valX}" "${valY}"

    [ ${status} -eq 1 ]
    [ -z "${output}" ]
  
  done
}

#***************************
# missing args
#***************************

@test "invoke is-larger-or-equal-integer - missing args" {

  run is-larger-or-equal-integer

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-larger-or-equal-integer 1
  
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke is-larger-or-equal-decimal - missing args" {

  run is-larger-or-equal-decimal

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-larger-or-equal-decimal 1
  
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

#***************************
# invalid args
#***************************

@test "invoke is-larger-or-equal-integer - invalid args" {

  local entry valX valY
  for entry in "${__is_larger_or_equal_invalid_decimal_list[@]}"; do

    read -r valX valY <<<"${entry}"
    run is-larger-or-equal-integer "${valX}" "${valY}"

    [ ${status} -eq 1 ]
    [ -z "${output}" ]

  done
}

@test "invoke is-larger-or-equal-decimal - invalid args" {

  local entry valX valY
  for entry in "${__is_larger_or_equal_invalid_decimal_list[@]}"; do

    read -r valX valY <<<"${entry}"
    run is-larger-or-equal-decimal "${valX}" "${valY}"

    [ ${status} -eq 1 ]
    [ -z "${output}" ]
  
  done
}

#***************************
# is-integer
#***************************

@test "invoke is-integer - true" {

  local intVal
  for intVal in "${__is_integer_true_list[@]}"; do
    run is-integer  "${intVal}"
    [ ${status} -eq 0 ]
    [ -z "${output}" ]
  done
}

@test "invoke is-integer - false" {

  local intVal
  for intVal in "${__is_integer_false_list[@]}"; do
    run is-integer  "${intVal}"
    [ ${status} -eq 1 ]
    [ -z "${output}" ]
  done
}

@test "invoke is-integer - invalid arguments - error" {

  run is-integer a

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-integer -a

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run is-integer --2
  
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-integer 1.4
  
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-integer 1,4
  
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke is-integer - no args" {
  run is-integer
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-integer ''
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-integer ' '
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

#***************************
# is-decimal
#***************************

@test "invoke is-decimal - true" {
  local decVal
  for decVal in "${__is_decimal_true_list[@]}"; do
    run is-decimal  "${decVal}"
    [ ${status} -eq 0 ]
    [ -z "${output}" ]
  done
  
  for decVal in "${__is_integer_true_list[@]}"; do
    run is-decimal  "${decVal}"
    [ ${status} -eq 0 ]
    [ -z "${output}" ]
  done
}

@test "invoke is-decimal - false" {
  local decVal
  for decVal in "${__is_decimal_false_list[@]}"; do
    run is-decimal  "${decVal}"
    [ ${status} -eq 1 ]
    [ -z "${output}" ]
  done
}

@test "invoke is-decimal - no args" {
  run is-decimal
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-decimal ''
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-decimal ' '
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

#***************************
# is-digit
#***************************

@test "invoke is-digit - true" {
  local digit
  for digit in "${__is_digit_true_list[@]}"; do  
    run is-digit "${digit}"
    [ ${status} -eq 0 ]
    [ -z "${output}" ]
  done
}

@test "invoke is-digit - false" {
  local digit
  for digit in "$ {__is_digit_false_list[@]}"; do
    run is-digit "${digit}"
    [ ${status} -eq 1 ]
    [ -z "${output}" ]
  done
}

@test "invoke is-digit - no args" {
  run is-digit
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-digit ''
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-digit ' '
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

#***************************
# is-integer-within-range
#***************************

@test "invoke is-integer-within-range - true" {
  run is-integer-within-range 1 1 10
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run is-integer-within-range 10 1 10
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run is-integer-within-range -1 -10 10
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run is-integer-within-range 0 -10 10
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run is-integer-within-range -10 -10 10
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run is-integer-within-range 10 -10 10
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke is-integer-within-range - false" {
  run is-integer-within-range 0 1 10
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-integer-within-range 11 1 10
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-integer-within-range -11 -10 10
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

#***************************
# is-integer-greater-zero
#***************************

@test "invoke is-integer-greater-zero - true" {
  local val
  for val in "${__is_larger_zero_integer_true_list[@]}"; do  
    run is-integer-greater-zero "${val}"
    [ ${status} -eq 0 ]
    [ -z "${output}" ]
  done
}

@test "invoke is-integer-greater-zero - false" {
  local val
  for val in "${__is_larger_zero_integer_false_list[@]}"; do  
    run is-integer-greater-zero "${val}"
    [ ${status} -eq 1 ]
    [ -z "${output}" ]
  done
}

@test "invoke is-integer-greater-zero - invalid" {
  skip 'how to handle invalid data - do we need a separate return value?'

  local val
  for val in "${__is_larger_zero_integer_invalid_list[@]}"; do  
    run is-integer-greater-zero "${val}"
    [ ${status} -eq 1 ]
    [ -z "${output}" ]
  done
}

@test "invoke is-integer-greater-zero - no args" {
  run is-integer-greater-zero
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-integer-greater-zero ''
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-integer-greater-zero ' '
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

#***************************
# is-integer-greater-one
#***************************

@test "invoke is-integer-greater-one" {

  run is-integer-greater-one 2

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run is-integer-greater-one 1
  
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke is-integer-greater-one - zero or negative values" {

  run is-integer-greater-one -2

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-integer-greater-one 0
  
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-integer-greater-one 1
  
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke is-integer-greater-one - missing arguments - error" {
  run is-integer-greater-one 

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke is-integer-greater-one - invalid arguments - error" {

  # no char
  run is-integer-greater-one -a

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  # no char
  run is-integer-greater-one a
  
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-integer-greater-one --2
  
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-integer-greater-one 1.4
  
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-integer-greater-one 1,4
  
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

#***************************
# is-positive-integer
#***************************

@test "invoke is-positive-integer - true" {
  local val
  for val in "${__is_positive_integer_true_list[@]}"; do  
    run is-positive-integer "${val}"
    [ ${status} -eq 0 ]
    [ -z "${output}" ]
  done
}

@test "invoke is-positive-integer - false" {
  local val
  for val in "${__is_positive_integer_false_list[@]}"; do  
    run is-positive-integer "${val}"
    [ ${status} -eq 1 ]
    [ -z "${output}" ]
  done
}

@test "invoke is-positive-integer - invalid" {
  skip 'how to handle invalid data - do we need a separate return value?'

  local val
  for val in "${__is_positive_integer_invalid_list[@]}"; do  
    run is-positive-integer "${val}"
    [ ${status} -eq 1 ]
    [ -z "${output}" ]
  done
}

@test "invoke is-positive-integer - no args" {
  run is-positive-integer
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-positive-integer ''
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-positive-integer ' '
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

#***************************
# strip-leading-digits
#***************************

@test "invoke strip-leading-digits - no strip" {
  run strip-leading-digits 4 123
  [ ${status} -eq 0 ]
  [ "${output}" == '0 123' ]
  
  run strip-leading-digits 4 .0123
  [ ${status} -eq 0 ]
  [ "${output}" == '0 .0123' ]
  
  run strip-leading-digits 4 -0123
  [ ${status} -eq 0 ]
  [ "${output}" == '0 -0123' ]
}

@test "invoke strip-leading-digits - strip" {
  run strip-leading-digits 1 123
  [ ${status} -eq 0 ]
  [ "${output}" == '1 23' ]
  
  run strip-leading-digits 1 111
  [ ${status} -eq 0 ]
  [ "${output}" == '3' ]
  
  run strip-leading-digits 0 000123
  [ ${status} -eq 0 ]
  [ "${output}" == '3 123' ]
}

@test "invoke strip-leading-digits - invalid args" {
  run strip-leading-digits
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run strip-leading-digits 1
  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run strip-leading-digits 123 123
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run strip-leading-digits 12 121212444
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke append-trailing-digits" {
  run append-trailing-digits 2 0 123
  [ ${status} -eq 0 ]
  [ "${output}" == '12300' ]
  
  run append-trailing-digits 1 10 111
  [ ${status} -eq 0 ]
  [ "${output}" == '11110' ]
  
  run append-trailing-digits 3 10 000123
  [ ${status} -eq 0 ]
  [ "${output}" == '000123101010' ]
}

@test "invoke append-trailing-digits - invalid args" {

  run append-trailing-digits

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run append-trailing-digits 2

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run append-trailing-digits 2 0

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run append-trailing-digits 2 -0 123

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run append-trailing-digits 2 -200 123

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run append-trailing-digits 2 a 123

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run append-trailing-digits 2 1.1 123

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run append-trailing-digits 0 0 123

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run append-trailing-digits -1 0 123

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run append-trailing-digits 0.1 0 123

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}