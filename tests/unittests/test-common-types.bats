#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-common.
#
# rmt-common is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-common is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-common. If not, see <https://www.gnu.org/licenses/>.

############################
# INCLUDES
############################

# currently, dotfiles to test reside in the same directory as bats files
declare -g -r __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/../../types.dotfile"

############################
# TESTS
############################

@test "invoke is-var - empty" {

  # no char
  run is-var

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  # no char
  run is-var ''

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  # only whitespaces
  run is-var '   '
  
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  # only tabs
  run is-var "$(printf '\t')"
  
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  # tabs and whitespaces
  run is-var "$(printf '  \t  ')"
  
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke is-var - no var" {

  # single char
  run is-var 'a'
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  # tabs and chars
  run is-var "$(printf ' a \t  ')"
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  local undef
  run is-var undef
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke is-var - no var - empty lists and maps" {

  local listA=()
  run is-var listA
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  declare -a listB=()
  run is-var listB
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  declare -A mapA=()
  run is-var mapA
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  declare -A mapA=( 
    [1]='a'
  )
  run is-var mapA
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke is-var - var" {
  
  local defA="defined"
  run is-var defA
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  local defB=
  run is-var defB
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  local empty=""
  run is-var empty
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke is-var - var - non empty lists" {

  local listA=(a)
  run is-var listA
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  declare -a listB=(a)
  run is-var listB
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke is-empty - empty" {

  # no char
  run is-empty

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  # no char
  run is-empty ''
  
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  # only whitespaces
  run is-empty '   '
  
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  # only tabs
  run is-empty "$(printf '\t')"
  
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  # tabs and whitespaces
  run is-empty "$(printf '  \t  ')"
  
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke is-empty - not empty" {

  # single char
  run is-empty 'a'

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  # tabs and chars
  run is-empty "$(printf ' a \t  ')"
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke contains-spaces - no spaces" {

  # single char
  run contains-spaces 'a'

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke contains-spaces - spaces" {
  
  run contains-spaces "$(printf ' a \t  ')"

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  run contains-spaces '   '

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run contains-spaces 'a b c'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}