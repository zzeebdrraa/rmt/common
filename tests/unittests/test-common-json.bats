#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-common.
#
# rmt-common is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-common is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-common. If not, see <https://www.gnu.org/licenses/>.

############################
# TODO
############################

# implement tests for:
#
# - has-fields-in-json-list-objects
# - has-fields-in-json-object
#
# extend existing tests for:
#
# - is-valid-json
# - get-value-from-json
# - get-value-from-json-file
# - is-json-type-filtered
# - is-json-type
#

############################
# INCLUDES
############################

# currently, dotfiles to test reside in the same directory as bats files
declare -g -r __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/../../json.dotfile"

############################
# HELPERS
############################

generate-test-data-json() {
  printf '%s' "
{
  \"name\":\"a name\",
  \"list\":[
    0,
    00,
    000
  ],
  \"obj\":{
    \"day\":\"today\",
    \"year\":3000,
    \"loop\":false
  }
}"
}

############################
# TEST DATA
############################

declare -r -g __test_data_dir=$(mktemp -d -u)
declare -r -g __test_data_json_file="${__test_data_dir}/test-data.json"

############################
# SETUP
############################

setup() {
  [[ -d "${__test_data_dir}" ]] && rm -r "${__test_data_dir}"
  mkdir -p "${__test_data_dir}"
}

teardown() {
  [[ -d "${__test_data_dir}" ]] && rm -r "${__test_data_dir}"
}

############################
# TESTS
############################

#***************************
# get-value-from-json
#***************************

@test "invoke get-value-from-json - no args" {

  generate-test-data-json >"${__test_data_json_file}"

  run get-value-from-json
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run get-value-from-json <"${__test_data_json_file}"
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke get-value-from-json - existing fieldname" {

  generate-test-data-json >"${__test_data_json_file}"

  run get-value-from-json 'name' <"${__test_data_json_file}"
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ "${output}" == "a name" ]
  
  run get-value-from-json 'list' <"${__test_data_json_file}"
  [ ${status} -eq 0 ]
  [ "${output}" == '[0,0,0]' ]
  
  run get-value-from-json 'obj' <"${__test_data_json_file}"
  [ ${status} -eq 0 ]
  [ "${output}" == '{"day":"today","year":3000,"loop":false}' ]
  
  run get-value-from-json 'obj.day' <"${__test_data_json_file}"
  [ ${status} -eq 0 ]
  [ "${output}" == 'today' ]
}

@test "invoke get-value-from-json - non-existing fieldname" {

  generate-test-data-json >"${__test_data_json_file}"

  run get-value-from-json 'unknown' <"${__test_data_json_file}"
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke get-value-from-json - invalid fieldname" {

  generate-test-data-json >"${__test_data_json_file}"

  run get-value-from-json ',' <"${__test_data_json_file}"
  [ ${status} -eq 1 ]
  # do no test output as it contains several jq error messages
  # [ -z "${output}" ]
  
  run get-value-from-json 'name.' <"${__test_data_json_file}"
  [ ${status} -eq 1 ]
  # do no test output as it contains several jq error messages
  # [ -z "${output}" ]
}

#***************************
# is-valid-json
#***************************

@test "invoke get-value-from-json - is-valid-json" {

  run is-valid-json <<<'{}'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run is-valid-json <<<'[]'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]  
}

@test "invoke get-value-from-json - is-valid-json - error" {

  run is-valid-json <<<'{'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  
  run is-valid-json <<<'}'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  
  run is-valid-json <<<'(}'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  
  run is-valid-json <<<'({}'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]

  run is-valid-json <<<'()'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  
  run is-valid-json <<< 's'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
}

@test "invoke get-value-from-json - is-valid-json - not clear" {
  skip "not clear yet how to handle"
  
  run is-valid-json <<<'1'
  printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  
  # no input blocks
  run is-valid-json
  printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  
  # an empty string is not detected as invalid json
  run is-valid-json <<<''
  printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

#***************************
# is-json-value
#***************************

@test "invoke get-value-from-json - is-json-value - match" {
  local -r jsonData='{"name":"test-name","address":{"street":"some street"},"id":123}'

  run is-json-value '.name' 'test-name' <<<"${jsonData}"
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  run is-json-value '.address.street' 'some street' <<<"${jsonData}"
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  run is-json-value '.address' '{"street":"some street"}' <<<"${jsonData}"
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run is-json-value '.id' '123' <<<"${jsonData}"
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke get-value-from-json - is-json-value - error - no args" {

  local -r jsonData='{"name":"test-name","address":{"street":"some street"},"id":123}'
  run is-json-value '' 'test-name' <<<"${jsonData}"

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-json-value '' 'test-name' <<<"${jsonData}"

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-json-value '' '' <<<"${jsonData}"

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke get-value-from-json - is-json-value - error - invalid json" {

  run is-json-value '.name' 'test-name' <<<'{}'

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json data has no field [.name]' ]

  run is-json-value '.name' 'test-name' <<<'{'

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json data has no field [.name]' ]
  
  run is-json-value '.name' 'test-name' <<<'}'

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json data has no field [.name]' ]
  
  run is-json-value '.name' 'test-name' <<<'()'

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json data has no field [.name]' ]
  
  run is-json-value '.name' 'test-name' <<< 's'

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json data has no field [.name]' ]
}

@test "invoke get-value-from-json - is-json-value - no field match" {
  local -r jsonData='{"name":"test-name","id":123}'

  run is-json-value '.location' 'test-location' <<<"${jsonData}"
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json data has no field [.location]' ]

  # missing dot in front of 'name'
  run is-json-value 'name' 'test-location' <<<"${jsonData}"
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  # TODO output is a bit unclear as one can easily miss the missing dot in front of the field name
  [ "${lines[0]}" == 'error: json data has no field [name]' ]

}

@test "invoke get-value-from-json - is-json-value - no value match" {
  local -r jsonData='{"name":"test-name","id":123}'

  run is-json-value '.name' 'test-location' <<<"${jsonData}"
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json field [.name] is not set to [test-location] but [test-name]' ]

  run is-json-value '.id' '1000' <<<"${jsonData}"
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json field [.id] is not set to [1000] but [123]' ]
}

#***************************
# is-json-type
#***************************

@test "invoke get-value-from-json - is-json-type - match" {

  run is-json-type 'object' <<<'{}'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  run is-json-type 'array' <<<'[]'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run is-json-type 'string' <<<'"text"'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run is-json-type 'number' <<<'1'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  local -r jsonData='{"name":"test-name","address":{"street":"some street"}}'
  
  run is-json-type 'string' < <(jq .address.street <<<"${jsonData}")

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke get-value-from-json - is-json-type - no match" {

  run is-json-type 'array' <<<'{}'

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run is-json-type 'string' <<<'[]'

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run is-json-type 'number' <<<'"text"'

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run is-json-type 'string' <<<'1'

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  local -r jsonData='{"name":"test-name","address":{"street":"some street"}}'
  
  run is-json-type 'object' < <(jq .address.street <<<"${jsonData}")

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke get-value-from-json - is-json-type - error - invalid json" {

  run is-json-type 'object' <<<'{'

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run is-json-type 'object' <<<'}'

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run is-json-type 'object' <<<'()'

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run is-json-type 'object' <<< 's'

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke get-value-from-json - is-json-type - error - no args" {
  
  run is-json-type '' <<<''

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-json-type 'object' <<<''

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  local -r jsonData='{"name":"test-name"}'
  
  run is-json-type '' <<<"${jsonData}"

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

#***************************
# is-json-type-verbose
#***************************

@test "invoke get-value-from-json - is-json-type-verbose - match" {

  run is-json-type-verbose 'object' <<<'{}'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  run is-json-type-verbose 'array' <<<'[]'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run is-json-type-verbose 'string' <<<'"text"'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run is-json-type-verbose 'number' <<<'1'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  local -r jsonData='{"name":"test-name","address":{"street":"some street"}}'
  
  run is-json-type-verbose 'string' < <(jq .address.street <<<"${jsonData}")

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke get-value-from-json - is-json-type-verbose - no match" {

  run is-json-type-verbose 'array' <<<'{}'

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object is not of type [array] but [object]' ]

  run is-json-type-verbose 'string' <<<'[]'

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object is not of type [string] but [array]' ]

  run is-json-type-verbose 'number' <<<'"text"'

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object is not of type [number] but [string]' ]

  run is-json-type-verbose 'string' <<<'1'

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object is not of type [string] but [number]' ]

  local -r jsonData='{"name":"test-name","address":{"street":"some street"}}'
  
  run is-json-type-verbose 'object' < <(jq .address.street <<<"${jsonData}")

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object is not of type [object] but [string]' ]
}

@test "invoke get-value-from-json - is-json-type-verbose - error - invalid json" {

  run is-json-type-verbose 'object' <<<'{'

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object could not be parsed' ]
  
  run is-json-type-verbose 'object' <<<'}'

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object could not be parsed' ]
  
  run is-json-type-verbose 'object' <<<'()'

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object could not be parsed' ]
  
  run is-json-type-verbose 'object' <<< 's'

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object could not be parsed' ]
}

@test "invoke get-value-from-json - is-json-type-verbose - error - no args" {
  
  run is-json-type-verbose '' <<<''

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-json-type-verbose 'object' <<<''

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: json object is not of type [object] but [null]' ]
  
  local -r jsonData='{"name":"test-name"}'
  
  run is-json-type-verbose '' <<<"${jsonData}"

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

#***************************
# is-json-type-filterd
#***************************

@test "invoke get-value-from-json - is-json-type-filtered - match" {

  run is-json-type-filtered 'object' '.address' <<<'{"address":{}}'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  run is-json-type-filtered 'array' '.' <<<'[]'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run is-json-type-filtered 'string' '.name' <<<'{"name":"test name"}'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run is-json-type-filtered 'number' '.housenumber' <<<'{"housenumber":1}'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  local -r jsonData='{"name":"test-name","address":{"street":"some street"}}'
  
  run is-json-type-filtered 'string' '.address.street' <<<"${jsonData}"

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke get-value-from-json - is-json-type-filtered - no match" {

  local -r jsonData='{"name":"test-name","address":{"street":"some street"}}'

  run is-json-type-filtered 'array' '.address' <<<"${jsonData}"

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run is-json-type-filtered 'string' '.address' <<<"${jsonData}"

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run is-json-type-filtered 'number' '.name' <<<"${jsonData}"

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run is-json-type-filtered 'string' '.address' <<<"${jsonData}"

  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run is-json-type-filtered 'object' '.address.street' <<<"${jsonData}"

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke get-value-from-json - is-json-type-filtered - error - invalid json" {

  run is-json-type-filtered 'object' '.address' <<<'{'
  
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-json-type-filtered 'object' '.address' <<<'}'

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-json-type-filtered 'object' '.address' <<<'()'

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-json-type-filtered 'object' '.address' <<< 's'

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke get-value-from-json - is-json-type-filtered - error - no args" {
  
  run is-json-type-filtered '' '.address' <<<''

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-json-type-filtered 'object' '.address' <<<''

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  local -r jsonData='{"name":"test-name"}'
  
  run is-json-type-filtered '' '.address' <<<"${jsonData}"

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

#***************************
# is-json-type-filtered-verbose
#***************************

@test "invoke get-value-from-json - is-json-type-filtered-verbose - match" {

  run is-json-type-filtered-verbose 'object' '.address' <<<'{"address":{}}'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  run is-json-type-filtered-verbose 'array' '.' <<<'[]'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run is-json-type-filtered-verbose 'string' '.name' <<<'{"name":"test name"}'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run is-json-type-filtered-verbose 'number' '.housenumber' <<<'{"housenumber":1}'

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  local -r jsonData='{"name":"test-name","address":{"street":"some street"}}'
  
  run is-json-type-filtered-verbose 'string' '.address.street' <<<"${jsonData}"

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke get-value-from-json - is-json-type-filtered-verbose - no match" {

  local -r jsonData='{"name":"test-name","address":{"street":"some street"}}'

  run is-json-type-filtered-verbose 'array' '.address' <<<"${jsonData}"

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: field [.address] of json object is not of type [array] but [object]' ]

  run is-json-type-filtered-verbose 'string' '.address' <<<"${jsonData}"

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: field [.address] of json object is not of type [string] but [object]' ]

  run is-json-type-filtered-verbose 'number' '.name' <<<"${jsonData}"

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: field [.name] of json object is not of type [number] but [string]' ]

  run is-json-type-filtered-verbose 'string' '.address' <<<"${jsonData}"

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: field [.address] of json object is not of type [string] but [object]' ]

  run is-json-type-filtered-verbose 'object' '.address.street' <<<"${jsonData}"

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: field [.address.street] of json object is not of type [object] but [string]' ]
}

@test "invoke get-value-from-json - is-json-type-filtered-verbose - error - invalid json" {

  run is-json-type-filtered-verbose 'object' '.address' <<<'{'
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object could not be parsed' ]
  
  run is-json-type-filtered-verbose 'object' '.address' <<<'}'

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object could not be parsed' ]
  
  run is-json-type-filtered-verbose 'object' '.address' <<<'()'

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object could not be parsed' ]
  
  run is-json-type-filtered-verbose 'object' '.address' <<< 's'

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object could not be parsed' ]
}

@test "invoke get-value-from-json - is-json-type-filtered-verbose - error - no args" {
  
  run is-json-type-filtered-verbose '' '.address' <<<''

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run is-json-type-filtered-verbose 'object' '.address' <<<''

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: field [.address] of json object is not of type [object] but [null]' ]
  
  local -r jsonData='{"name":"test-name"}'
  
  run is-json-type-filtered-verbose '' '.address' <<<"${jsonData}"

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

#***************************
# has-fields-in-json-object
#***************************

@test "invoke get-value-from-json - has-fields-in-json-object - match" {

  local -r jsonData='{"name":"test-name","address":{"street":"some street"}}'

  run has-fields-in-json-object 'name' 'address' <<<"${jsonData}"
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke get-value-from-json - has-fields-in-json-object - no match" {

  local -r jsonData='{"name":"test-name","address":{"street":"some street"}}'

  run has-fields-in-json-object 'unknown' <<<"${jsonData}"
  # printf '# l %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: json object has no keys ["unknown"]' ]

  run has-fields-in-json-object 'address.street' <<<"${jsonData}"
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: json object has no keys ["address.street"]' ]
}

@test "invoke get-value-from-json - has-fields-in-json-object - no valid object" {

  # a simple list
  local jsonData='["name", "test-name", "address", "street", "some street"]'

  run has-fields-in-json-object 'name' <<<"${jsonData}"
  # printf '# l1 %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: json object could not be parsed' ]

  # a list of objects
  local jsonData='[{"name":"test-name","address":{"street":"some street"}}]'

  run has-fields-in-json-object 'address.street' <<<"${jsonData}"
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: json object could not be parsed' ]
}

@test "invoke get-value-from-json - has-fields-in-json-object - empty object" {

  run has-fields-in-json-object 'name' <<<'{}'
  # printf '# l1 %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: json object has no keys ["name"]' ]
}

#***************************
# has-fields-in-json-object-in-list
#***************************

@test "invoke get-value-from-json - has-fields-in-json-object-in-list - match" {

  local -r jsonData='[{"name":"one-name","address":{"street":"some street"}},{"name":"another-name","address":{}}]'

  run has-fields-in-json-object-in-list 'address' <<<"${jsonData}"
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  run has-fields-in-json-object-in-list 'name' 'address' <<<"${jsonData}"
  # printf '# l %s\n' "${lines[@]}" >&3

  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke get-value-from-json - has-fields-in-json-object-in-list - no match" {

  local -r jsonData='[{"name":"one-name","address":{"street":"some street"}},{"language":"it","name":"another-name","address":{}}]'

  run has-fields-in-json-object-in-list 'address.street' <<<"${jsonData}"
  # printf '# l1 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object has no keys ["address.street"]' ]
  [ "${lines[1]}" == 'error: json object has no keys ["address.street"]' ]
  
  # only one object in list has a langiage field
  run has-fields-in-json-object-in-list 'language' <<<"${jsonData}"
  # printf '# l2 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object has no keys ["language"]' ]
}

@test "invoke get-value-from-json - has-fields-in-json-object-in-list - no object list" {

  # a list of strings
  local jsonData='["name", "test-name", "address", "street", "some street"]'

  run has-fields-in-json-object-in-list 'name' <<<"${jsonData}"
  # printf '# l1 %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object is not of type [object] but [string]' ]
  [ "${lines[1]}" == 'error: json object is not of type [object] but [string]' ]
  [ "${lines[2]}" == 'error: json object is not of type [object] but [string]' ]
  [ "${lines[3]}" == 'error: json object is not of type [object] but [string]' ]
  [ "${lines[4]}" == 'error: json object is not of type [object] but [string]' ]
  
  # a list of nulls
  local jsonData='[null, null, null]'

  run has-fields-in-json-object-in-list 'name' <<<"${jsonData}"
  # printf '# l2 %s\n' "${lines[@]}" >&3
  
  [ "${lines[0]}" == 'error: json object is not of type [object] but [null]' ]
  [ "${lines[1]}" == 'error: json object is not of type [object] but [null]' ]
  [ "${lines[2]}" == 'error: json object is not of type [object] but [null]' ]

  # a plain object
  local jsonData='{"name":"test-name","address":{"street":"some street"}}'

  run has-fields-in-json-object-in-list 'name' <<<"${jsonData}"
  # printf '# l3 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: json object is not of type [array] but [object]' ]
  
  # null
  run has-fields-in-json-object-in-list 'name' <<< 'null'
  # printf '# l4 %s\n' "${lines[@]}" >&3

  [ ${status} -eq 1 ]
  [ "${output}" == 'error: json object is not of type [array] but [null]' ]
}

@test "invoke get-value-from-json - has-fields-in-json-object-in-list - empty list" {

  # an empty list
  run has-fields-in-json-object-in-list 'name' <<<'[]'
  # printf '# l1 %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object is not of type [object] but [null]' ]

  # a list of empty objects
  run has-fields-in-json-object-in-list 'name' <<<'[{},{}]'
  # printf '# l2 %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: json object has no keys ["name"]' ]
  [ "${lines[1]}" == 'error: json object has no keys ["name"]' ]
}