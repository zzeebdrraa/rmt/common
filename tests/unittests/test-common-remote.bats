#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-common.
#
# rmt-common is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-common is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-common. If not, see <https://www.gnu.org/licenses/>.

############################
# TODO
############################

# add tests for
#
#   - reboot-host-su-exec
#   - reboot-host-dry
#   - exec-cmd-on-host-dry
#   - copy-file-to-host-dry


############################
# INCLUDES
############################

# currently, dotfiles to test reside in the same directory as bats files
declare -r -g __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/../../math.dotfile"
load "${__source_dir}/../../strings.dotfile"
load "${__source_dir}/../../input.dotfile"
load "${__source_dir}/../../remote.dotfile"
load "${__source_dir}/../../../common-test/test-rmt-asserts-common"

############################
# MOCKS
############################

scp-mock() {
  printf 'scp-mock\n'
  printf '%s\n' "$@"
  return 0
}

ssh-mock() {
  printf 'ssh-mock\n'
  printf '%s\n' "$@"
  return 0
}

sshpass-mock() {
  printf 'sshpass\n'
  printf '%s\n' "$@"
  return 0
}


############################
# SETUP
############################

setup() {
  __ssh_cmd=ssh-mock
  __scp_cmd=scp-mock
  __sshpass_cmd=sshpass-mock
}

############################
# TESTS
############################

@test "invoke copy-file-to-host - single host - with key and pwd" {

  run copy-file-to-host "${__source_dir}/test-common-remote.bats" '~/dest-dir/dest-file' 'someuser' 'example.com' '22' 'silent' <<EOF
key path/to/sshkey
pwd somepwd
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ "${status}" -eq 0 ]
  
  assert-lines-equal lines <<EOF
sshpass
-P
ass
-d
3
scp-mock
-o
StrictHostKeyChecking=no
-o
UserKnownHostsFile=/dev/null
-o
ConnectionAttempts=200
-i
path/to/sshkey
-P
22
${__source_dir}/test-common-remote.bats
someuser@example.com:~/dest-dir/dest-file
EOF
}

@test "invoke copy-file-to-host - single host - with pwd only" {

  run copy-file-to-host "${__source_dir}/test-common-remote.bats" '~/dest-dir/dest-file' 'someuser' 'example.com' '22' 'silent' <<EOF
pwd somepwd
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ "${status}" -eq 0 ]
  
  assert-lines-equal lines <<EOF
sshpass
-P
ass
-d
3
scp-mock
-o
StrictHostKeyChecking=no
-o
UserKnownHostsFile=/dev/null
-o
ConnectionAttempts=200
-P
22
${__source_dir}/test-common-remote.bats
someuser@example.com:~/dest-dir/dest-file
EOF
}

@test "invoke copy-file-to-host - single host - with key only" {

  run copy-file-to-host "${__source_dir}/test-common-remote.bats" '~/dest-dir/dest-file' 'someuser' 'example.com' '22' 'silent' <<EOF
key path/to/sshkey
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ "${status}" -eq 0 ]
  
  assert-lines-equal lines <<EOF
scp-mock
-o
StrictHostKeyChecking=no
-o
UserKnownHostsFile=/dev/null
-o
ConnectionAttempts=200
-i
path/to/sshkey
-P
22
${__source_dir}/test-common-remote.bats
someuser@example.com:~/dest-dir/dest-file
EOF
}

@test "invoke copy-file-to-host - single host - no key and pwd" {

  run copy-file-to-host "${__source_dir}/test-common-remote.bats" '~/dest-dir/dest-file' 'someuser' 'example.com' '22' 'silent'

  # printf '# l %s\n' "${lines[@]}" >&3
  [ "${status}" -eq 0 ]
  
  # NOTE scp port argument is -P
  assert-lines-equal lines <<EOF
scp-mock
-o
StrictHostKeyChecking=no
-o
UserKnownHostsFile=/dev/null
-o
ConnectionAttempts=200
-P
22
${__source_dir}/test-common-remote.bats
someuser@example.com:~/dest-dir/dest-file
EOF
}

@test "invoke exec-cmd-on-host - single host - with key and pwd" {

  run exec-cmd-on-host 'some command on remote host' 'someuser' 'example.com' '22' 'silent' <<EOF
key path/to/sshkey
pwd somepwd
EOF

  [ "${status}" -eq 0 ]

  # NOTE ssh port argument is -p
  assert-lines-equal lines <<EOF
sshpass
-P
ass
-d
3
ssh-mock
-o
StrictHostKeyChecking=no
-o
UserKnownHostsFile=/dev/null
-o
ConnectionAttempts=200
-i
path/to/sshkey
-p
22
someuser@example.com
some command on remote host
EOF
}

@test "invoke exec-cmd-on-host - single host - with pwd only" {

  run exec-cmd-on-host 'some command on remote host' 'someuser' 'example.com' '22' 'silent' <<EOF
pwd somepwd
EOF

  [ "${status}" -eq 0 ]

  assert-lines-equal lines <<EOF
sshpass
-P
ass
-d
3
ssh-mock
-o
StrictHostKeyChecking=no
-o
UserKnownHostsFile=/dev/null
-o
ConnectionAttempts=200
-p
22
someuser@example.com
some command on remote host
EOF
}

@test "invoke exec-cmd-on-host - single host - with key only" {

  run exec-cmd-on-host 'some command on remote host' 'someuser' 'example.com' '22' 'silent' <<EOF
key path/to/sshkey
EOF

  [ "${status}" -eq 0 ]

  assert-lines-equal lines <<EOF
ssh-mock
-o
StrictHostKeyChecking=no
-o
UserKnownHostsFile=/dev/null
-o
ConnectionAttempts=200
-i
path/to/sshkey
-p
22
someuser@example.com
some command on remote host
EOF
}

@test "invoke exec-cmd-on-host - single host - no key and pwd" {

  run exec-cmd-on-host 'some command on remote host' 'someuser' 'example.com' '22' 'silent'

  [ "${status}" -eq 0 ]

  assert-lines-equal lines <<EOF
ssh-mock
-o
StrictHostKeyChecking=no
-o
UserKnownHostsFile=/dev/null
-o
ConnectionAttempts=200
-p
22
someuser@example.com
some command on remote host
EOF
}

@test "invoke reboot-host - single host - with key and pwd" {
  run reboot-host 'someuser' 'example.com' '22' 'silent' <<EOF
key path/to/sshkey
pwd somepwd
EOF
  
  # printf '# l %s\n' "${lines[@]}" >&3
  [ "${status}" -eq 0 ]
  
    assert-lines-equal lines <<EOF
sshpass
-P
ass
-d
3
ssh-mock
-o
StrictHostKeyChecking=no
-o
UserKnownHostsFile=/dev/null
-o
ConnectionAttempts=200
-i
path/to/sshkey
-p
22
someuser@example.com
nohup reboot & >/dev/null ; exit
EOF

}

@test "invoke reboot-host - single host - with pwd only" {
  run reboot-host 'someuser' 'example.com' '22' 'silent' <<EOF
pwd somepwd
EOF
  
  # printf '# l %s\n' "${lines[@]}" >&3
  [ "${status}" -eq 0 ]
  
    assert-lines-equal lines <<EOF
sshpass
-P
ass
-d
3
ssh-mock
-o
StrictHostKeyChecking=no
-o
UserKnownHostsFile=/dev/null
-o
ConnectionAttempts=200
-p
22
someuser@example.com
nohup reboot & >/dev/null ; exit
EOF

}

@test "invoke reboot-host - single host - with key only" {
  run reboot-host 'someuser' 'example.com' '22' 'silent' <<EOF
key path/to/sshkey
EOF
  
  # printf '# l %s\n' "${lines[@]}" >&3
  [ "${status}" -eq 0 ]
  
    assert-lines-equal lines <<EOF
ssh-mock
-o
StrictHostKeyChecking=no
-o
UserKnownHostsFile=/dev/null
-o
ConnectionAttempts=200
-i
path/to/sshkey
-p
22
someuser@example.com
nohup reboot & >/dev/null ; exit
EOF

}

@test "invoke reboot-host - single host - no key and pwd" {
  run reboot-host 'someuser' 'example.com' '22' 'silent'
  
  # printf '# l %s\n' "${lines[@]}" >&3
  [ "${status}" -eq 0 ]
  
    assert-lines-equal lines <<EOF
ssh-mock
-o
StrictHostKeyChecking=no
-o
UserKnownHostsFile=/dev/null
-o
ConnectionAttempts=200
-p
22
someuser@example.com
nohup reboot & >/dev/null ; exit
EOF

}

@test "invoke reboot-host-su - single host - with key and pwd" {
  run reboot-host-su 'someuser' 'example.com' '22' 'silent' <<EOF
key path/to/sshkey
pwd somepwd
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ "${status}" -eq 0 ]
  
  assert-lines-equal lines <<EOF
sshpass
-P
ass
-d
3
ssh-mock
-o
StrictHostKeyChecking=no
-o
UserKnownHostsFile=/dev/null
-o
ConnectionAttempts=200
-i
path/to/sshkey
-p
22
someuser@example.com
nohup sudo reboot & >/dev/null ; exit
EOF

}

@test "invoke reboot-host-su - single host - with key only" {
  run reboot-host-su 'someuser' 'example.com' '22' 'silent' <<EOF
key path/to/sshkey
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ "${status}" -eq 0 ]
  
  assert-lines-equal lines <<EOF
ssh-mock
-o
StrictHostKeyChecking=no
-o
UserKnownHostsFile=/dev/null
-o
ConnectionAttempts=200
-i
path/to/sshkey
-p
22
someuser@example.com
nohup sudo reboot & >/dev/null ; exit
EOF

}

@test "invoke reboot-host-su - single host - with pwd only" {
  run reboot-host-su 'someuser' 'example.com' '22' 'silent' <<EOF
pwd somepwd
EOF

  # printf '# l %s\n' "${lines[@]}" >&3
  [ "${status}" -eq 0 ]
  
  assert-lines-equal lines <<EOF
sshpass
-P
ass
-d
3
ssh-mock
-o
StrictHostKeyChecking=no
-o
UserKnownHostsFile=/dev/null
-o
ConnectionAttempts=200
-p
22
someuser@example.com
nohup sudo reboot & >/dev/null ; exit
EOF

}

@test "invoke reboot-host-su - single host - no key and pwd" {
  run reboot-host-su 'someuser' 'example.com' '22' 'silent'

  # printf '# l %s\n' "${lines[@]}" >&3
  [ "${status}" -eq 0 ]
  
  assert-lines-equal lines <<EOF
ssh-mock
-o
StrictHostKeyChecking=no
-o
UserKnownHostsFile=/dev/null
-o
ConnectionAttempts=200
-p
22
someuser@example.com
nohup sudo reboot & >/dev/null ; exit
EOF

}