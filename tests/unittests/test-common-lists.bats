#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-common.
#
# rmt-common is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-common is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-common. If not, see <https://www.gnu.org/licenses/>.

############################
# INCLUDES
############################

# currently, dotfiles to test reside in the same directory as bats files
declare -g -r __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/../../lists.dotfile"

############################
# TESTS
############################

@test "invoke remove-list-dublicates" {
  local listIn=(
    1 2 3 12 3 1 7 5 5 7
  )
  local listOut=() res=0
  
  ! remove-list-dublicates listIn listOut && res=1
  [ ${res} -eq 0 ]
  [ ${#listOut[*]} -eq 6 ]
  [ "${listOut[*]}" == '1 2 3 12 7 5' ]
}

@test "invoke remove-list-dublicates - no data" {
  local listIn=() listOut=() res=0
  
  ! remove-list-dublicates listIn listOut && res=1
  [ ${res} -eq 0 ]
  [ ${#listOut[*]} -eq 0 ]
  [ -z "${listOut[*]}" ]
}

@test "invoke remove-list-dublicates - no args" {
  run remove-list-dublicates
  [ ${status} -eq 1 ]
  
  local -r listIn=()
  run remove-list-dublicates listIn
  [ ${status} -eq 1 ]
}