#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-common.
#
# rmt-common is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-common is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-common. If not, see <https://www.gnu.org/licenses/>.

############################
# INCLUDES
############################

# currently, dotfiles to test reside in the same directory as bats files
declare -g -r __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/../../getters.dotfile"

############################
# TEST DATA
############################

declare -r -g __test_data_dir=$(mktemp -d -u)

declare -r -g __get_port_from_uri_invalid_port_list=(
  'https://example.com:-80 -80'
  'https://example.com:--80 --80'
  'https://example.com:::80 ::80'
  'https://example.com:a80 a80'
)

declare -r -g __get_port_from_uri_out_of_bounds_port_list=(
  'https://example.com:0 0'
  'https://example.com:65536 65536'
)

declare -r -g __get_port_from_uri_valid_list=(
  'http://example.com 80'
  'http://example.com:1000 1000'
  'https://example.com:10000 10000'
  'https://example.com:442/ 442'
  'https://example.com:442/api/end 442'
  'https://www.example.org:81 81'
  'http://127.0.0.1 80'
  'http://127.0.0.1:1000 1000'
  'https://127.0.0.1:10000 10000'
  'https://127.0.0.1:442/ 442'
  'https://127.0.0.1:442/api/end 442'
)

declare -r -g __get_host_from_uri_valid_list=(
  'http://example.com http://example.com'
  'https://example.com:442 https://example.com'
  'https://example.com:442/ https://example.com'
  'https://example.com:442/api/end https://example.com'
  'http://127.0.0.1 http://127.0.0.1'
  'http://127.0.0.1:442 http://127.0.0.1'
  'http://127.0.0.1:442/ http://127.0.0.1'
  'http://127.0.0.1:442/api/end http://127.0.0.1'
)

declare -r -g invalid_uri_list=(
  'null'
  'file://test-common-utils.bats'
)

declare -r -g __invalid_protocol_list=(
  'https:/example.com https:/'
  'httpsss://example.com httpsss://'
  'httpsss://example.com httpsss://'
  'http http'
  'https https'
  'https: https:'
  'https:: https::'
  'https:/ https:/'
  'https:/// https:///'
  'http:://example.com http:://'
  'http:////example.com http:////'
  'http:/// http:///'
  'https:/// https:///'
)

declare -r -g __not_yet_detected_host_list=(
  '\n'
  'http://example;'
  'http://-example'
  'http://:example'
  'http:// '
)

declare -r -g __invalid_host_list=(
  'http://'
)

############################
# SETUP
############################

setup() {
  [[ -d "${__test_data_dir}" ]] && rm -r "${__test_data_dir}"
  mkdir -p "${__test_data_dir}"
}

teardown() {
  [[ -d "${__test_data_dir}" ]] && rm -r "${__test_data_dir}"
}

############################
# TESTS
############################

#***************************
# get-value-from-var-indirect
#***************************

@test "invoke get-value-from-var-indirect - no args" {

  run get-value-from-var-indirect
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run get-value-from-var-indirect ''
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke get-value-from-var-indirect - invalid args" {

  skip 'not yet handled'

  run get-value-from-var-indirect '  '
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

#***************************
# get-value-from-var-indirect
#***************************

@test "invoke get-value-from-var-indirect - existing var" {

  declare -r -g __a_global_var='global var'

  run get-value-from-var-indirect __a_global_var
  [ ${status} -eq 0 ]
  [ "${output}" == 'global var' ]
  
  local -r aLocalVar='local var'
  
  run get-value-from-var-indirect aLocalVar
  [ ${status} -eq 0 ]
  [ "${output}" == 'local var' ]
}

@test "invoke get-value-from-var-indirect - non-existing var" {

  run get-value-from-var-indirect unknownVar
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke get-port-from-uri - no args" {

  run get-port-from-uri
  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run get-port-from-uri ''
  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run get-port-from-uri '   '
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

#***************************
# get-port-from-uri
#***************************

@test "invoke get-port-from-uri - invalid uri" {
  local uri
  for uri in "${invalid_uri_list[@]}"; do
    run get-port-from-uri "${uri}"
    [ ${status} -eq 1 ]
    [ "${output}" == "error: uri [${uri}] not valid" ]
  done
}

@test "invoke get-port-from-uri - invalid protocol" {
  local entry uri result
  for entry in "${__invalid_protocol_list[@]}"; do
    read -r uri result <<<"${entry}"
    run get-port-from-uri "${uri}"
    [ ${status} -eq 1 ]
    [ "${output}" == "error: invalid protocol [${result}] in uri [${uri}]" ]
  done
}

@test "invoke get-port-from-uri - invalid port number" {
  local entry uri result
  for entry in "${__get_port_from_uri_invalid_port_list[@]}"; do
    read -r uri result <<<"${entry}"
    run get-port-from-uri "${uri}"
    [ ${status} -eq 1 ]
    [ "${output}" == "error: invalid port number [${result}]" ]
  done
}

@test "invoke get-port-from-uri - out of bounds port number" {
  local entry uri result
  for entry in "${__get_port_from_uri_out_of_bounds_port_list[@]}"; do
    read -r uri result <<<"${entry}"
    run get-port-from-uri "${uri}"
    [ ${status} -eq 1 ]
    [ "${output}" == "error: port number [${result}] out of bounds" ]
  done
}

@test "invoke get-port-from-uri" {
  local entry uri result
  for entry in "${__get_port_from_uri_valid_list[@]}"; do
    read -r uri result <<<"${entry}"
    run get-port-from-uri "${uri}"
    [ ${status} -eq 0 ]
    [ "${output}" == "${result}" ]
  done
}

@test "invoke get-port-from-uri - errors not yet detected" {  
  skip 'errors not yet detected'

  run get-port-from-uri '\n'
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke get-port-from-uri - uri not yet detected" {
  skip 'uris not yet detected'

  run get-port-from-uri 'example.com:442'
  [ ${status} -eq 0 ]
  [ "${output}" == '442' ]
  
  run get-port-from-uri 'www.example.com:442'
  [ ${status} -eq 0 ]
  [ "${output}" == '442' ]
}

#***************************
# get-host-from-uri
#***************************

@test "invoke get-host-from-uri - no args" {
  run get-host-from-uri
  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run get-host-from-uri ''
  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run get-host-from-uri '  '
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke get-host-from-uri - invalid uri" {
  local entry uri result
  for entry in "${invalid_uri_list[@]}"; do
    read -r uri result <<<"${entry}"
    run get-host-from-uri "${uri}"
    [ ${status} -eq 1 ]
    [ "${output}" == "error: uri [${uri}] not valid" ]
  done
}

@test "invoke get-host-from-uri - invalid protocol" {
  local entry uri result
  for entry in "${__invalid_protocol_list[@]}"; do
    read -r uri result <<<"${entry}"
    run get-host-from-uri "${uri}"
    # printf '# l %s\n' "${lines[@]}" >&3
    [ ${status} -eq 1 ]
    [ "${output}" == "error: invalid protocol [${result}] in uri [${uri}]" ]
  done
}

@test "invoke get-host-from-uri - invalid host" {  
  local uri
  for uri in "${__invalid_host_list[@]}"; do
    run get-host-from-uri "${uri}"
    # printf '# l %s\n' "${lines[@]}" >&3
    [ ${status} -eq 1 ]
    [ "${output}" == "error: no host available in uri [${uri}]" ]
  done
}

@test "invoke get-host-from-uri - errors not yet detected" {  
  skip 'errors not yet detected'

  local uri
  for uri in "${__not_yet_detected_host_list[@]}"; do
    run get-host-from-uri "${uri}"
    [ ${status} -eq 1 ]
    [ "${output}" == "error: no host available in uri [${uri}]" ]
  done
}

@test "invoke get-host-from-uri" {
  local entry uri result
  for entry in "${__get_host_from_uri_valid_list[@]}"; do
    read -r uri result <<<"${entry}"
    run get-host-from-uri "${uri}"
    [ ${status} -eq 0 ]
    [ "${output}" == "${result}" ]
  done
}

#***************************
# get-port-from-uri-stdin
#***************************

@test "invoke get-port-from-uri-stdin" {
  local entry uri result
  for entry in "${__get_port_from_uri_valid_list[@]}"; do
    read -r uri result <<<"${entry}"
    run get-port-from-uri-stdin <<<"${uri}"
    [ ${status} -eq 0 ]
    [ "${output}" == "${result}" ]
  done
}

@test "invoke get-host-from-uri-stdin" {
  local entry uri result
  for entry in "${__get_host_from_uri_valid_list[@]}"; do
    read -r uri result <<<"${entry}"
    run get-host-from-uri-stdin <<<"${uri}"
    [ ${status} -eq 0 ]
    [ "${output}" == "${result}" ]
  done
}

@test "invoke get-host-from-uri-stdin - invalid host" {  
  local uri
  for uri in "${__invalid_host_list[@]}"; do
    run get-host-from-uri-stdin <<<"${uri}"
    [ ${status} -eq 1 ]
    [ "${output}" == "error: no host available in uri [${uri}]" ]
  done
}

@test "invoke get-host-from-uri-stdin - invalid uri" {
  local entry uri result
  for entry in "${invalid_uri_list[@]}"; do
    read -r uri result <<<"${entry}"
    run get-host-from-uri-stdin <<<"${uri}"
    [ ${status} -eq 1 ]
    [ "${output}" == "error: uri [${uri}] not valid" ]
  done
}

@test "invoke get-host-from-uri-stdin - invalid protocol" {
  local entry uri result
  for entry in "${__invalid_protocol_list[@]}"; do
    read -r uri result <<<"${entry}"
    run get-host-from-uri-stdin <<< "${uri}"
    [ ${status} -eq 1 ]
    [ "${output}" == "error: invalid protocol [${result}] in uri [${uri}]" ]
  done
}