#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-common.
#
# rmt-common is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-common is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-common. If not, see <https://www.gnu.org/licenses/>.

############################
# INCLUDES
############################

# currently, dotfiles to test reside in the same directory as bats files
declare -r __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/../../parallel.dotfile"
load "${__source_dir}/../../../common-test/test-rmt-mocks-common.bash"

############################
# TEST DATA
############################

############################
# MOCKS
############################

mock-parallel-function-error-two-of-three() {
  case "$1" in
    key1)
      return 1
      ;;
    key2)
      printf '%s\n' "$*"
      ;;
    key3)
      return 1
      ;;
  esac
  return 0
}

############################
# ASSERTS
############################

assert-match-unordered() {
  declare -r -n givenListRef="$1"
  
  local expectedLine givenLine counterNoMatch
  local linesNotMatchingList=()
  
  while read -r expectedLine; do
    counterNoMatch=0
    for givenLine in "${givenListRef[@]}"; do
      [[ ! "${givenLine}" =~ ${expectedLine} ]] && counterNoMatch=$((counterNoMatch+1))
    done
    
    [[ ${counterNoMatch} -eq ${#givenListRef[*]} ]] && linesNotMatchingList+=("${expectedLine}")
  done

  [[ ${#linesNotMatchingList[*]} -eq 0 ]] && return 0

  printf '%s\n' "${linesNotMatchingList[@]}"
  return 1
}

############################
# UTILTITIES
############################

# a wrapper function to print the number of connection retries
# stored in global variable _netcat_counter
run-parallel-wrapper() {
  ! run-parallel "$@" && return 1
  return 0
}

############################
# CALLBACKS
############################

test-get-start-msg() {
  printf 'create host [%s] ...\n' "${2}"
}

test-get-wait-msg() {
  printf 'wait until host creation has been completed for [%s] [%s] ...\n' "${2}" "${3}"
}

test-get-error-msg() {
  printf 'host creation failed for [%s] [%s]\n' "${2}" "${3}"
}

############################
# SETUP
############################

setup() {
  __run_parallel_get_start_msg=parallel-generate-start-msg
  __run_parallel_get_wait_msg=parallel-generate-wait-msg
  __run_parallel_get_error_msg=parallel-generate-error-msg
}

############################
# TESTS
############################

@test "invoke run-parallel" {

  local funcKeyList=("key1")
  local funcArgList=("arg1 arg2 arg3")
  local isVerbose=0

  # run one function only
  run run-parallel-wrapper ${isVerbose} funcKeyList mock-cmd-ok-print-args "${funcArgList[@]}"
  [ "${status}" -eq 0 ]
  [ "${lines[0]}" == 'key1 arg1 arg2 arg3' ]

  # run three functions
  funcKeyList=('key1' 'key2' 'key3')
  run run-parallel-wrapper ${isVerbose} funcKeyList mock-cmd-ok-print-args "${funcArgList[@]}"

  [ "${status}" -eq 0 ]
  run assert-match-unordered lines <<EOF
key1 arg1 arg2 arg3
key2 arg1 arg2 arg3
key3 arg1 arg2 arg3
EOF
  
  [ -z "${output}" ]
  [ ${status} -eq 0 ]
  
  # run one function, no function arguments
  funcKeyList=('key1')
  run run-parallel-wrapper ${isVerbose} funcKeyList mock-cmd-ok-print-args

  [ "${status}" -eq 0 ]
  [ "${lines[0]}" == 'key1' ]
}

@test "invoke run-parallel - verbose" {

  local funcKeyList=("key1")
  local funcArgList=("arg1 arg2 arg3")
  local isVerbose=1

  # run one function only
  run run-parallel-wrapper ${isVerbose} funcKeyList mock-cmd-ok-print-args "${funcArgList[@]}"
  [ "${status}" -eq 0 ]

  # order not defined
  run assert-match-unordered lines <<EOF
debug: invoke mock-cmd-ok-print-args \[key1\] \.\.\.
key1 arg1 arg2 arg3
(debug: wait for mock-cmd-ok-print-args \[key1\] \[[0-9]+\] \.\.\.)
EOF

  [ -z "${output}" ]
  [ ${status} -eq 0 ]

  # run three functions
  funcKeyList=('key1' 'key2' 'key3')
  run run-parallel-wrapper ${isVerbose} funcKeyList mock-cmd-ok-print-args "${funcArgList[@]}"

  run assert-match-unordered lines <<EOF
debug: invoke mock-cmd-ok-print-args \[key1\] \.\.\.
debug: invoke mock-cmd-ok-print-args \[key2\] \.\.\.
debug: invoke mock-cmd-ok-print-args \[key3\] \.\.\.
key1 arg1 arg2 arg3
key2 arg1 arg2 arg3
key3 arg1 arg2 arg3
(debug: wait for mock-cmd-ok-print-args \[key1\] \[[0-9]+\] \.\.\.)
(debug: wait for mock-cmd-ok-print-args \[key2\] \[[0-9]+\] \.\.\.)
(debug: wait for mock-cmd-ok-print-args \[key3\] \[[0-9]+\] \.\.\.)
EOF
  
  [ -z "${output}" ]
  [ ${status} -eq 0 ]
}

@test "invoke run-parallel - missing args - error" {

  local -r isVerbose=0

  run run-parallel-wrapper
  [ "${status}" -eq 1 ]
  [ -z "${output}" ]

  run run-parallel-wrapper ${isVerbose}
  [ "${status}" -eq 1 ]
  [ -z "${output}" ]

  run run-parallel-wrapper ${isVerbose} mock-cmd-ok-print-args
  [ "${status}" -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke run-parallel - missing args - error - verbose" {

  # has not effect yet
  local -r isVerbose=1

  run run-parallel-wrapper
  [ "${status}" -eq 1 ]
  [ -z "${output}" ]

  run run-parallel-wrapper ${isVerbose}
  [ "${status}" -eq 1 ]
  [ -z "${output}" ]

  run run-parallel-wrapper ${isVerbose} mock-cmd-ok-print-args
  [ "${status}" -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke run-parallel - error" {
  local funcKeyList=("key1")
  local funcArgList=("arg1 arg2 arg3")
  local isVerbose=0

  run run-parallel-wrapper ${isVerbose} funcKeyList mock-cmd-error "$funcArgList[@]"
  [ "${status}" -eq 1 ]
  [[ "${lines[0]}" =~ (error: mock-cmd-error failed \[key1\] \[[0-9]+\] \.\.\.) ]]
  
  # run three functions
  funcKeyList=('key1' 'key2' 'key3')
  run run-parallel-wrapper ${isVerbose} funcKeyList mock-parallel-function-error-two-of-three "$funcArgList[@]"
  [ "${status}" -eq 1 ]
  
  run assert-match-unordered lines <<EOF
(error: mock-parallel-function-error-two-of-three failed \[key1\] \[[0-9]+\] \.\.\.)
key2 arg1 arg2 arg3
(error: mock-parallel-function-error-two-of-three failed \[key3\] \[[0-9]+\] \.\.\.)
EOF

  [ -z "${output}" ]
  [ ${status} -eq 0 ]
}

@test "invoke run-parallel - error - verbose" {
  local funcKeyList=("key1")
  local funcArgList=("arg1 arg2 arg3")
  local isVerbose=1

  run run-parallel-wrapper ${isVerbose} funcKeyList mock-cmd-error "$funcArgList[@]"
  [ "${status}" -eq 1 ]

  run assert-match-unordered lines <<EOF
(debug: invoke mock-cmd-error \[key1\] \.\.\.)
(debug: wait for mock-cmd-error \[key1\] \[[0-9]+\] \.\.\.)
(error: mock-cmd-error failed \[key1\] \[[0-9]+\] \.\.\.)
EOF

  [ -z "${output}" ]
  [ ${status} -eq 0 ]
  
  # run three functions
  funcKeyList=('key1' 'key2' 'key3')
  run run-parallel-wrapper ${isVerbose} funcKeyList mock-parallel-function-error-two-of-three "$funcArgList[@]"
  [ "${status}" -eq 1 ]
  
  run assert-match-unordered lines <<EOF
(debug: invoke mock-parallel-function-error-two-of-three \[key1\] \.\.\.)
(debug: invoke mock-parallel-function-error-two-of-three \[key2\] \.\.\.)
(debug: invoke mock-parallel-function-error-two-of-three \[key3\] \.\.\.)
(debug: wait for mock-parallel-function-error-two-of-three \[key1\] \[[0-9]+\] \.\.\.)
(debug: wait for mock-parallel-function-error-two-of-three \[key2\] \[[0-9]+\] \.\.\.)
(debug: wait for mock-parallel-function-error-two-of-three \[key3\] \[[0-9]+\] \.\.\.)
(error: mock-parallel-function-error-two-of-three failed \[key1\] \[[0-9]+\] \.\.\.)
(error: mock-parallel-function-error-two-of-three failed \[key3\] \[[0-9]+\] \.\.\.)
key2 arg1 arg2 arg3
EOF

  [ -z "${output}" ]
  [ ${status} -eq 0 ]
}

@test "invoke run-parallel - verbose - custom " {

  __run_parallel_get_start_msg=test-get-start-msg
  __run_parallel_get_wait_msg=test-get-wait-msg
  __run_parallel_get_error_msg=test-get-error-msg

  local funcKeyList=("hostname")
  local funcArgList=("arg1 arg2 arg3")
  local isVerbose=1

  # run one function only
  run run-parallel-wrapper ${isVerbose} funcKeyList mock-cmd-ok-print-args "${funcArgList[@]}"
  [ "${status}" -eq 0 ]

  run assert-match-unordered lines <<EOF
debug: create host \[hostname\] \.\.\.
hostname arg1 arg2 arg3
(debug: wait until host creation has been completed for \[hostname\] \[[0-9]+\] \.\.\.)
EOF

  [ -z "${output}" ]
  [ ${status} -eq 0 ]
}

@test "invoke run-parallel - error - custom" {
  __run_parallel_get_start_msg=test-get-start-msg
  __run_parallel_get_wait_msg=test-get-wait-msg
  __run_parallel_get_error_msg=test-get-error-msg
  
  local funcKeyList=("hostname")
  local funcArgList=("arg1 arg2 arg3")
  local isVerbose=0

  run run-parallel-wrapper ${isVerbose} funcKeyList mock-cmd-error "$funcArgList[@]"
  [ "${status}" -eq 1 ]
  [[ "${lines[0]}" =~ (error: host creation failed for \[hostname\] \[[0-9]+\]) ]]
}

@test "invoke run-parallel - error - verbose - custom" {
  __run_parallel_get_start_msg=test-get-start-msg
  __run_parallel_get_wait_msg=test-get-wait-msg
  __run_parallel_get_error_msg=test-get-error-msg
  
  local funcKeyList=("hostname")
  local funcArgList=("arg1 arg2 arg3")
  local isVerbose=1

  run run-parallel-wrapper ${isVerbose} funcKeyList mock-cmd-error "$funcArgList[@]"
  [ "${status}" -eq 1 ]
  # [[ "${lines[0]}" =~ (error: mock-cmd-error failed \[hostname\] \[[0-9]+\] \.\.\.) ]]

  run assert-match-unordered lines <<EOF
debug: create host \[hostname\]
(debug: wait until host creation has been completed for \[hostname\] \[[0-9]+\])
(error: host creation failed for \[hostname\] \[[0-9]+\])
EOF

  [ -z "${output}" ]
  [ ${status} -eq 0 ]
}