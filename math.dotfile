#!/usr/bin/env bash

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-common.
#
# rmt-common is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-common is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-common. If not, see <https://www.gnu.org/licenses/>.

############################
# FUNCTIONS
############################

#
# detects if a given value is a integer number
#
# @param $1 - a integer number
#
#   can be positive or negative
#
# @return 0 - if value is a integer number
# @return 1 - if value is not a integer number or no value has been provided
#
is-integer() {
  [[ "$1" =~ ^[-]{0,1}[0-9]+$ ]] && return 0
  return 1
}

#
# detects if a given value is a positive integer number
#
# @param $1 - a integer number greater equal zero
#
#   $1 >= 0
#
# @return 0 - if value is a positive integer number
# @return 1 - if value is not a positive integer number or no value has been provided
#
is-positive-integer() {
  [[ "$1" =~ ^[0-9][0-9]*$ ]] && return 0
  return 1
}

#
# detects if a given value is a positive integer number larger than 0
#
# @param $1 - a integer number
#
#   $1 > 0
#
# @return 0 - if value is a positive integer number larger than 0
# @return 1 - if value is not a positive integer number larger than 0 or no value has been provided
#
is-integer-greater-zero() {
  [[ "$1" =~ ^[1-9][0-9]*$ ]] && return 0
  return 1
}

# @param $1 value to test
#
# @return 0 value is a positive integer number >0
# @return 1 value is not a positiv integer 
is-integer-greater-one() {
  [[ "$1" =~ ^[2-9]+[0-9]*$ ]] && return 0
  return 1
}

is-integer-within-range() {
  ! is-integer "$1" || ! is-integer "$2" ! is-integer "$3" && return 1
  [[ "$1" -lt "$2" ]] && return 1
  [[ "$1" -gt "$3" ]] && return 1
  return 0
}

#
# detects if a given value is a single digit in the value range [0-9]
#
# @param $1 - a single digit
#
# @return 0 - if value is a single digit
# @return 1 - if value is not a single digit or no value has been provided
#
is-digit() {
  [[ "$1" =~ ^[0-9]$ ]] && return 0
  return 1
}

#
# detects if a given value is a decimal number
#
# NOTE also works with plain integer numbers. the used regex is just
#   a bit more complicated than for plain integer numbers.
#
# @param $1 - a decimal number
#
#   can be positive or negative
#
# @return 0 - if value is a decimal number
# @return 1 - if value is not a decimal number or no value has been provided
#
is-decimal() {
  [[ "$1" =~ ^([-]{0,1}[0-9]+|[-]{0,1}[.][0-9]+|[-]{0,1}[0-9]+[.][0-9]*)$ ]] && return 0
  return 1
}

#
# remove one digit or a consecutive number of the same digit from the begin
# if a positive number.
#
# @param $1 - a single digit to be removed
#
#   is expected to be a positive number 0 <= $1 <= 9
#
# @param $2 - the number from where to remove the leading digit(s)
#
# @output - prints result to stdout on a single line
#
#   line is formatted as:
#
#     <num-stripped-digits> <value>
#
#   where
#
#     <num-stripped-digits> is the number of stripped digits from <value>
#       is 0 if no digits have been stripped
#       is >0 if digit(s) have been stripped
#
#     <value> is the original value if no digit(s) have been stripped
#       or the stripped value
#       or empty if all digits hev been stripped
#
# @return 0 - no error occured
# @return 1 - an error occured
#
strip-leading-digits() {
  [[ -z "$1" ]] || [[ -z "$2" ]] && return 1
  ! is-digit "$1" && return 1

  local -r digit="$1"
  local -r value="$2"
  local -r lenValue=${#value}

  local i=0 valueStripped
  for ((; i<lenValue; ++i)); do
    [[ "${value:i:1}" =~ [.-] ]] && valueStripped="${value}" && break
    [[ ${value:i:1} -eq ${digit} ]] && [[ "${#valueStripped}" -eq 0 ]] && continue
    valueStripped="${value:i}"
    break
  done

  if [[ -z "${valueStripped}" ]]; then
    printf '%s' "$i"
  else
    printf '%s %s' "$i" "${valueStripped}" 
  fi
  return 0
}

#
# append one or more digit(s) to a number value.
#
# @param $1 - the number of copies of given digit to be appended
#
#   is expected to be a positive number $1 >= 1
#
# @param $2 - a single or multi digit sequence to be appended
#
#   is expected to be a sequence of positive digits >= 0
#
# @param $3 - the number where to append the trailing digit(s)
#
# @output - prints result to stdout on a single line
#
#   line is formatted as:
#
#     <value>
#
#   where
#
#     <value> is the original value plus appended digit(s)
#
# @return 0 - no error occured
# @return 1 - an error occured
#
append-trailing-digits() {
  [[ -z "$1" ]] || [[ -z "$2" ]] || [[ -z "$3" ]] && return 1
  ! is-integer-greater-zero "$1" && return 1
  ! is-positive-integer "$2" && return 1

  local -r numDigits="$1"
  local -r digit="$2"
  local value="$3"

  local i
  for ((i=0; i<numDigits; ++i)); do
    value+="${digit}"
  done

  printf '%s' "${value}"
  return 0
}

#
# detects if a integer number A is larger or equal a integer number B
#
#   A >= B
#
# @param $1 - integer number A
#
#   can be positive or negative
#
# @param $2 - integer number B
#
#   can be positive or negative
#
# @return 0 - if A >= B
# @return 1 - if A < B or if one or both numbers are no integer values
#
is-larger-or-equal-integer() {
  ! is-integer "$1" || ! is-integer "$2" && return 1
  [[ $1 -ge $2 ]] && return 0
  return 1
}

#
# detects if a decimal number A is larger or equal a decimal number B
#
#   A >= B
#
# NOTE also works with plain integer numbers but the comparison
#   algorithm performs some additional operations before being
#   able to detect if values to compare are plain integers.
#
# @param $1 - decimal number A
#
#   can be positive or negative
#
# @param $2 - decimal number B
#
#   can be positive or negative
#
# @return 0 - if A >= B
# @return 1 - if A < B or if one or both numbers are no decimal values
#
is-larger-or-equal-decimal() {
 # set -x
  [[ -z "$1" ]] || [[ -z "$2" ]] && return 1

  ! is-decimal "$1" || ! is-decimal "$2" && return 1

  local decimalA fractionA
  local decimalB fractionB

  IFS='.' read -r decimalA fractionA <<<"$1"
  IFS='.' read -r decimalB fractionB <<<"$2"

  local isValANegative=0 isValBNegative=0

  [[ "${decimalA:0:1}" == '-' ]] && {
    # strip leading -
    decimalA=${decimalA:1}
    isValANegative=1
  }
  [[ "${decimalB:0:1}" == '-' ]] && {
    # strip leading -
    decimalB=${decimalB:1}
    isValBNegative=1
  }

  [[ -z "${decimalA}" ]] && decimalA=0
  [[ -z "${fractionA}" ]] && fractionA=0
  [[ -z "${decimalB}" ]] && decimalB=0
  [[ -z "${fractionB}" ]] && fractionB=0

  [[ ${isValANegative} -eq 1 ]] && {
    [[ ${decimalA} -eq 0 ]] && [[ ${fractionA} -eq 0 ]] && isValANegative=0
  }
  [[ ${isValBNegative} -eq 1 ]] && {
    [[ ${decimalB} -eq 0 ]] && [[ ${fractionB} -eq 0 ]] && isValBNegative=0
  }

  # if only one value is negative, then we do not need to compare any further
  # NOTE does not apply for a negative zero
  [[ ${isValANegative} -eq 1 ]] && [[ ${isValBNegative} -eq 0 ]] && return 0
  [[ ${isValANegative} -eq 0 ]] && [[ ${isValBNegative} -eq 1 ]] && return 1

  # from here on, both values are either negative or positive
  local resGreater=0 resLess=1

  # if both values are negative we invert the return values as result of
  # negative only value comparison is the inverse of positive only value comparison
  [[ ${isValANegative} -eq 1 ]] && [[ ${isValBNegative} -eq 1 ]] && {
    resGreater=1
    resLess=0
  }

  [[ ${decimalA} -lt ${decimalB} ]] && return ${resLess}
  [[ ${decimalA} -gt ${decimalB} ]] && return ${resGreater}

  # from here on we know that decimalA == decimalB

  # this case also covers -0 == 0, -0 == -0, 0 == -0
  [[ ${fractionA} =~ ^[-][0]+$ ]] && [[ ${fractionB} =~ ^[-][0]+$ ]] && return 0

  [[ ${fractionA:0:1} -ne 0 ]] && [[ ${fractionB:0:1} -ne 0 ]] && {
    # both fraction values do not start with a 0
    # ie fractionA=.12345 fractionB=.98765
    [[ ${fractionA} -lt ${fractionB}  ]] && return ${resLess}
    return ${resGreater}
  }

  # from here on we know that one or both fraction values do start with a 0
  # ie fractionA=.098 fractionB=.00987
  local numZerosFractionA numZerosFractionB
  local fractionAStripped fractionBStripped

  read -r numZerosFractionA fractionAStripped < <(strip-leading-digits 0 "${fractionA}")
  read -r numZerosFractionB fractionBStripped < <(strip-leading-digits 0 "${fractionB}")

  # the fraction that has more leading zeros is by definition the smaller one  
  [[ ${numZerosFractionA} -gt ${numZerosFractionB} ]] && return ${resLess}
  [[ ${numZerosFractionA} -lt ${numZerosFractionB} ]] && return ${resGreater}

  # from here on we know that both fraction values have the same number of leading zeros 
  # ie fractionA=.0098 fractionB=.00980
  # ie fractionA=.0098 fractionB=.00981
  local -r lenFractionAStripped=${#fractionAStripped}
  local -r lenFractionBStripped=${#fractionBStripped}

  [[ ${lenFractionAStripped} -lt ${lenFractionBStripped} ]] && \
    read -r fractionAStripped < <(append-trailing-digits $((lenFractionBStripped-lenFractionAStripped)) 0 "${fractionAStripped}"  )

  [[ ${lenFractionBStripped} -gt ${lenFractionBStripped} ]] && \
    read -r fractionBStripped < <(append-trailing-digits $((lenFractionAStripped-lenFractionBStripped)) 0 "${fractionBStripped}"  )

  # now, both fraction have the same length and can be compared
  [[ ${fractionAStripped} -eq ${fractionBStripped} ]] && return 0
  [[ ${fractionAStripped} -lt ${fractionBStripped} ]] && return ${resLess}
  return ${resGreater}
# set +x
}
