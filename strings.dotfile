#!/usr/bin/env bash

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-common.
#
# rmt-common is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-common is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-common. If not, see <https://www.gnu.org/licenses/>.

############################
# DEPENDENCIES
############################

# the following file must have been sourced before using functions of this file:
#
# - types.dotfile
# - math.dotfile
#

############################
# FUNCTIONS
############################

# remove leading and trailing chars from value if value contains 
#   a paricular char as its first and last characters. if not, value
#   stays as-is.
#
# @param $1 the value to be trimmed
# @param $2 the char to be removed. only a single char is currently handled.
#
# return 0 prints the new value to stdout
#
#   - if original value contains trim chars, chars are removed from value begin and end
#   - if original value does not contain trim chars, the original value remains as-is
#
# return 1 nothing os written to stdout
#
#   - value or trim char are not provided
#   - trim char is not a single char
#
symmetrically-trim-char() {
  is-empty "$1" || is-empty "$2" && return 1
  [[ "${#2}" -gt 1 ]] && return 1

  local value="$1"
  local -r char="$2"

  local  -r lenValue=${#value}
  # get 1 char starting from first char => first char
  local -r firstChar="${value:0:1}"
  # get last 1 char => last char
  local -r lastChar="${value:$((lenValue-1)):1}"

  # only trim if first and last chars are the expected chars to trim
  [[ "${firstChar}" == "${char}" ]] && [[ "${lastChar}" == "${char}" ]] && {
    # get the substring starting from char 2 until last-1
    value="${value:1:-1}"
  }
  [[ "${firstChar}" == "${char}" ]] && [[ "${lastChar}" != "${char}" ]] && {
    # get the substring starting from char 2 until the end
    value="${value:1:}"
  }
    [[ "${firstChar}" != "${char}" ]] && [[ "${lastChar}" == "${char}" ]] && {
    # get the substring starting from char 1 until last-1
    value="${value:0:-1}"
  }

  printf '%s' "${value}"
  return 0
}

# trims leading and trailing double-quotes from variable values
#   and writes back the trimmed result to those variables.
#
# NOTE expects variable names insted of string values. that means that 
#   variables whose names are passed to this function must have been
#   defined before calling this function. the values of the given
#   variables will be retrieved via parameter indirection.
#
# @param $1 ... the names of the variables to be trimmed
#
#   the variable values are obtained trough parameter indirection.
#   at least 1 variable name must be povided and that variable must
#   already exist.
#
# NOTE variables are supposed to hold string values and are not supposed
#   to be lists or maps
#
# return 0 variable values have been trimmed or remain as they are
# return 1 no variable name has been provided
#
symmetrically-trim-quotes-indirection() {
# set -x
  is-empty "$*" && return 1

  local value varName
  for varName in "$@"; do
    is-empty "${varName}" || is-integer "${varName}" || ! is-var "${varName}" || starts-with "${varName}" '-' && return 1
    # retrieve variable value via parameter indirection of varName
    ! {
      value=$(symmetrically-trim-char "${!varName}" '"')
    } && return 1

    # overwrite existig variable referenced via varName
    printf -v "${varName}" "%s" "${value}"
  done
# set +x
  return 0
}

# @param $1 value to test
# @param $2 pattern to match at value begin
#
#   can be a regular expresion, a single character or a string
#
# @return 0 value starts with given pattern
# @return 1 value does not start with given pattern 
#
#   or value or pattern are empty
starts-with() {
  is-empty "$1" || is-empty "$2" && return 1
  [[ "$1" =~ ^$2  ]] && return 0
  return 1
}

# this is a concat fucntion
# concat '/' a b c d => a/b/c/d
#
# @param $1 delimiter
#
#   the textual representation of a delimiter between two parts.
#
#   can be empty, a single char or a char sequence.
#
# @param $2... parts to concatenate
#
#   at least one part must be given, otherwise function stops
#     with an error.
#
# @out
#
#   if no error occured, the concatenated string will be written
#     to stdout
#
# @return 0
#
#   given parts have been concatenated
#
# @return 1
#
#   - no arguments have been provided at all
#   - only a delimiter has been provided, but no single part at all
#
concat-str() {
# set -x
  [[ $# -eq 0 ]] && return 1
  [[ $# -eq 1 ]] && return 0
  [[ $# -eq 2 ]] && {
    printf '%s' "$2"
    return 0
  }

  local -r delimiter="$1"
  shift

  local part outStr
  if [[ ${#delimiter} -gt 0 ]]; then
    for part in "$@"; do
      [[ -z "${part}" ]] && continue
      outStr+="${part}${delimiter}"
    done

    # remove last delimiter in concatenated output string
    [[ "${#outStr}" -gt 0 ]] && outStr="${outStr:0:-${#delimiter}}"
  else
    for part in "$@"; do
      [[ -z "${part}" ]] && continue
      outStr+="${part}"
    done
  fi

    printf '%s' "${outStr}"
# set +x
  return 0
}

# the same as concat-str except that it adds a newline at the end of the
#   concatenated string
concat-str-newline() {
  ! concat-str "$@" && return 1
  printf '\n'
  return 0
}

# @param $1... one or more arguments to test
#
# @return 0
#
#   all provided arguments are non.empty
#
# @return 1
#
#   at least one of the provided arguments are a empty string
is-one-of-str-empty() {
  local str
  for str in "$@"; do
    [[ -z "${str}" ]] && return 0
  done
  return 1
}
