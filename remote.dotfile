#!/usr/bin/env bash

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-common.
#
# rmt-common is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-common is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-common. If not, see <https://www.gnu.org/licenses/>.

############################
# DEPENDENCIES
############################

# the followig external programs are being used
#
# - ssh and scp are being used for remote host command invocation and processing
# - sshpass is being used when password is read from stdin and is then passed to scp or ssh

############################
# GLOBAL DATA
############################

declare -g -r __ssh_common_options_list=(
  '-o' 'StrictHostKeyChecking=no' 
  '-o' 'UserKnownHostsFile=/dev/null'
  '-o' 'ConnectionAttempts=200'
)

declare -g __ssh_cmd=ssh
declare -g __scp_cmd=scp
declare -g __sshpass_cmd=sshpass

declare -g -r __common_remote_ssh_port_default=22
declare -g -r __common_remote_output_format_default='silent'

############################
# PRINT CALLBACKS
############################

print-copy-cmd-info() {
  local -r fileLocal="$1"
  local -r fileRemote="$2"
  local -r username="$3"
  local -r host="$4"
  local -r port="$5"
  local -r sshKey="$6"
  local -r password="$7"

  printf \
'copy file to host
--------------
  file (local)        : %s
  file (remote)       : %s
  user                : %s
  host                : %s
  ssh-key (optional)  : %s
  password (optional) : %s
--------------
' "${fileLocal}" "${fileRemote}" "${username}" "${host}:${port}" "${sshKey}" "${password//?/x}"

  return 0
}

print-exec-cmd-info() {
  local -r cmdRemote="$1"
  local -r username="$2"
  local -r host="$3"
  local -r port="$4"
  local -r sshKey="$5"
  local -r password="$6"

  printf \
'run cmd on host
--------------
  remote command      : %s
  user                : %s
  host                : %s
  ssh-key (optional)  : %s
  password (optional) : %s
--------------
' "${cmdRemote}" "${username}" "${host}:${port}" "${sshKey}" "${password//?/x}"

  return 0
}

print-exec-cmd-error() {
  local -r sshCmd="$1"
  local -r commonOps="$2"
  local -r moreOps="$3"
  local -r userHost="$4"
  local -r remoteCmd="$5"

  printf \
'error: command could not be run on remote host
--------------
%s %s %s %s %s
--------------
' "${sshCmd}" "${commonOps}" "${moreOps}" "${userHost}" "${remoteCmd}"

  return 0
}

print-copy-cmd-error() {
  local -r sshCmd="$1"
  local -r commonOpts="$2"
  local -r moreOpts="$3"
  local -r fileLocal="$4"
  local -r fileRemote="$5"

  printf \
'error: command could not be run on remote host
--------------
%s %s %s %s %s
--------------
' "${sshCmd}" "${commonOpts}" "${moreOpts}" "${fileLocal}" "${fileRemote}"

  return 0
}

############################
# FUNCTIONS
############################

copy-file-to-host() {
  local -r fileLocal="$1"
  local -r fileRemote="$2"
  local -r username="$3"
  local -r host="$4"
  # optional
  local -r port="${5:-${__common_remote_ssh_port_default}}"
  local -r outputFormat="${6:-${__common_remote_output_format_default}}"

  [[ -z "${host}" ]] || [[ -z "${username}" ]] && return 1
  [[ ! -f "${fileLocal}" ]] && {
    printf 'error: local file [%s] does not exist .. skipping\n' "${fileLocal}" >&2
    return 1
  }

  declare -A secDataMap=()
  read-lines-key-value-from-stdin secDataMap 0.01

  local sshOptionsList=() result=0 sshVerbosity
  local -r sshKey="${secDataMap[key]}" password="${secDataMap[pwd]}"

  [[ -n "${sshKey}" ]] && sshOptionsList+=('-i' "${sshKey}") 
  [[ -n "${sshVerbosity}" ]] && sshOptionsList+=("${sshVerbosity}")

  sshOptionsList+=('-P' "${port}")

  [[ "${outputFormat}" != "silent" ]] && print-copy-cmd-info "${fileLocal}" "${fileRemote}" "${username}" "${host}" "${port}" "${sshKey}" "${password}"

  if [ -n "${password}" ]; then
    ! ${__sshpass_cmd} -P ass -d 3 3< <(printf "%s" "${password}") "${__scp_cmd}" "${__ssh_common_options_list[@]}" "${sshOptionsList[@]}" "${fileLocal}" "${username}@${host}:${fileRemote}" && result=1
  else
    ! ${__scp_cmd} "${__ssh_common_options_list[@]}" "${sshOptionsList[@]}" "${fileLocal}" "${username}@${host}:${fileRemote}" && result=1
  fi

  [[ ${result} -ne 0 ]] && print-copy-cmd-error "${__scp_cmd}" "${__ssh_common_options_list[*]}" "${sshOptionsList[*]}" "${fileLocal}" "${username}@${host}:${fileRemote}" >&2
  return ${result}
}

copy-file-to-host-dry() {
  local -r fileLocal="$1"
  local -r fileRemote="$2"
  local -r username="$3"
  local -r host="$4"
  # optional
  local -r port="${5:-${__common_remote_ssh_port_default}}"
  local -r outputFormat="${6:-${__common_remote_output_format_default}}"

  declare -A secDataMap=()
  read-lines-key-value-from-stdin secDataMap 0.01
  local -r sshKey="${secDataMap[key]}" password="${secDataMap[pwd]}"

  local sshOptionsList=() result=0 sshVerbosity
  [[ -n "${sshKey}" ]] && sshOptionsList+=('-i' "${sshKey}") 
  [[ -n "${sshVerbosity}" ]] && sshOptionsList+=("${sshVerbosity}")
  sshOptionsList+=('-P' "${port}")

  local cmd
  if [ -n "${password}" ]; then
    cmd="${__sshpass_cmd} -P ass -d 3 ${__scp_cmd} ${__ssh_common_options_list[*]} ${sshOptionsList[*]} ${fileLocal} ${username}@${host}:${fileRemote}"
  else
    cmd="${__scp_cmd} ${__ssh_common_options_list[*]} ${sshOptionsList[*]} ${fileLocal} ${username}@${host}:${fileRemote}"
  fi
  printf '%s\n' "${cmd}"
  return 0
}

exec-cmd-on-host() {
  local -r cmdRemote="$1"
  local -r username="$2"
  local -r host="$3"
  # optional
  local -r port="${4:-${__common_remote_ssh_port_default}}"
  local -r outputFormat="${5:-${__common_remote_output_format_default}}"

  [[ -z "${cmdRemote}" ]] || [[ -z "${host}" ]] || [[ -z "${username}" ]] && return 1

  declare -A secDataMap=()
  read-lines-key-value-from-stdin secDataMap 0.01

  local sshOptionsList=() result=0 sshVerbosity
  local -r sshKey="${secDataMap[key]}" password="${secDataMap[pwd]}"

  [[ -n "${sshKey}" ]] && sshOptionsList+=('-i' "${sshKey}") 
  [[ -n "${sshVerbosity}" ]] && sshOptionsList+=("${sshVerbosity}")
  sshOptionsList+=('-p' "${port}")

  [[ "${outputFormat}" != "silent" ]] && print-exec-cmd-info "${cmdRemote}" "${username}" "${host}" "${port}" "${sshKey}" "${password}"

  if [ -n "${password}" ]; then
    ! ${__sshpass_cmd} -P ass -d 3 3< <(printf "%s" "${password}") "${__ssh_cmd}" "${__ssh_common_options_list[@]}" "${sshOptionsList[@]}" "${username}@${host}" "${cmdRemote}" && result=1
  else
    ! ${__ssh_cmd} "${__ssh_common_options_list[@]}" "${sshOptionsList[@]}" "${username}@${host}" "${cmdRemote}" && result=1
  fi

  [[ ${result} -ne 0 ]] && print-exec-cmd-error "${__ssh_cmd}" "${__ssh_common_options_list[*]}" "${sshOptionsList[*]}" "${username}@${host}" "${cmdRemote}" >&2
  return ${result}
}

exec-cmd-on-host-dry() {
  local -r cmdRemote="$1"
  local -r username="$2"
  local -r host="$3"
  # optional
  local -r port="${4:-${__common_remote_ssh_port_default}}"
  local -r outputFormat="${5:-${__common_remote_output_format_default}}"

  [[ -z "${cmdRemote}" ]] || [[ -z "${host}" ]] || [[ -z "${username}" ]] && return 1

  declare -A secDataMap=()
  read-lines-key-value-from-stdin secDataMap 0.01

  local sshOptionsList=() result=0 sshVerbosity
  local -r sshKey="${secDataMap[key]}" password="${secDataMap[pwd]}"

  [[ -n "${sshKey}" ]] && sshOptionsList+=('-i' "${sshKey}") 
  [[ -n "${sshVerbosity}" ]] && sshOptionsList+=("${sshVerbosity}")
  sshOptionsList+=('-P' "${port}")

  local cmd
  if [ -n "${password}" ]; then
    cmd="${__sshpass_cmd} -P ass -d 3 ${__ssh_cmd} ${__ssh_common_options_list[*]} ${sshOptionsList[*]} ${username}@${host} ${cmdRemote}"
  else
    cmd="${__ssh_cmd} ${__ssh_common_options_list[@]} ${sshOptionsList[@]} ${username}@${host} ${cmdRemote}"
  fi
  printf '%s\n' "${cmd}"
  return 0
}

# only works when exec-cmd-on-host is logging in on host as root
reboot-host() {
  ! exec-cmd-on-host 'nohup reboot & >/dev/null ; exit' "$@" && return 1
  return 0
}

reboot-host-dry() {
  ! exec-cmd-on-host-dry 'nohup reboot & >/dev/null ; exit' "$@" && return 1
  return 0
}

# only works if sudo is available on host
reboot-host-su() {
  ! exec-cmd-on-host 'nohup sudo reboot & >/dev/null ; exit' "$@" && return 1
  return 0
}

reboot-host-su-exec() {
  ! exec-cmd-on-host-exec 'nohup sudo reboot & >/dev/null ; exit' "$@" && return 1
  return 0
}
