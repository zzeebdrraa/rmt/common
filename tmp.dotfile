#!/usr/bin/env bash

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-common.
#
# rmt-common is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-common is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-common. If not, see <https://www.gnu.org/licenses/>.

############################
# DEPENDENCIES
############################

# the followig external programs are being used
#
# - mktemp
#

############################
# GLOBAL DATA
############################

declare -g __mktemp_cmd='mktemp'

############################
# FUNCTIONS
############################

make-tmp-dir() {
  local tmpDir
  ! {
    tmpDir=$(${__mktemp_cmd} --quiet --directory)
  } && return 1

  printf '%s' "${tmpDir}"
  return 0
}

make-tmp-dir-path() {
  local tmpDir
  ! {
    tmpDir=$(${__mktemp_cmd} --quiet --directory --dry-run)
  } && return 1

  printf '%s' "${tmpDir}"
  return 0
}
