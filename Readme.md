# rmt-common

A small library written in bash of common functionality used by rmt-tools.

## Dependencies

- fifo.dotfile     : mkfifo, math.dotfile
- input.dotfile    : stty, math.dotfile, strings.dotfile
- json.dotfile     : jq
- lists.dotfile      : types.dotfile
- map.dotfile      : types.dotfile
- math.dotfile     : *none*
- net.dotfile      : netcat, math.dotfile
- parallel.dotfile : *none*
- passwords.dotfile: pwgen
- remote.dotfile   : ssh, scp, sshpass
- strings.dotfile  : math.dotfile, types.dotfile
- tmp.dotfile      : mktemp
- types.dotfile    : *none*
- utils.dotfile    : json.dotfile, getters.dotfile

## Installation

TO BE DONE

## Testing

```bash
cd tests/unittests
# run all bats tests in this directory
bats .
```

## Usage

import any library component via bats `source` command, from within your script or from command line

```bash
source remote.dotfile
```

## ToDo

- install/uninstall via `make`
- run tests via `make`
- extract testing taxonomy in order to create a generic taxonomy for future testcases
- run code coverage of bats tests with [bashcov](https://github.com/infertux/bashcov)

## License

[GPL3](https://www.gnu.org/licenses/gpl-3.0.html#license-text)
